# 说明
此为老结构,新结构请查看
> 新结构: https://gitee.com/liufeiyu92/amt_pytest_allure_new
# 环境
* Python 3.7.4
* Pycharm 2019.1.2
* JDK 1.8.0_221

# 安装依赖库
* 运行目录下的 InitPackage.bat 进行依赖包安装

# 运行测试
* 运行 main.bat 进行测试(确保已经安装好依赖包和环境)

# 目录结构
>Common (封装公共方法)

>Data (测试数据)

>Log (日志)

>Settings (Config配置模块)

>ProjectEnum (封装枚举类)

>Templates (静态网页)

>Report (测试报告)

>TestCase (测试用例)

>.coveragerc (测试代码覆盖率插件配置文件<不统计某些目录>)

>filename.txt (依赖包列表)

>JenkinsRun.py (提供给jenkins运行的主入口)

>RunMain.py (程序主入口)

>pytest.ini (配置文件)

>InitPackage.bat (初始化安装依赖包)

>main.bat (命令行运行主入口)