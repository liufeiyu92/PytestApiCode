"""
@File    : JenkinsRun.py
@Time    : 2019/8/16 10:19
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import pytest
from Common.ConnectJira import *
import Common.Consts
from Settings import Config

conf = Config.Config()
xml_report_path = r'E:\Software\Jenkins\workspace\test_result\allure-reports'
args = ['-s', '-q', '--no-print-logs', '--alluredir', xml_report_path]  # pytest运行参数
pytest.main(args)
if conf.jira_issue_switch == "1":
    # 自动提交问题单,如果错误接口数量超过10,则不自动提交问题单，否则自动提交问题单
    error_num = Common.Consts.API_RESPONSE_NUMBER
    true_num = Common.Consts.API_TRUE_RESPONSE_NUMBER
    accuracy_rate = "%.2f" % (true_num / (true_num + error_num))
    if Common.Consts.API_RESPONSE_NUMBER >= 10 or float(accuracy_rate) < 0.95:
        pass
    else:
        for error in Common.Consts.API_ERROR_RESPONSE_DATA:
            GetJiraData().create_issue(
                project_id=error['project_id'],
                summary=error['summary'],
                description=error['description'],
                assignee=error['assignee'])
if __name__ == '__main__':
    pass

