"""
@File    : demo1.py
@Time    : 2019/8/7 10:00
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import asyncio
import sys


async def foo():
    print('----start foo')
    await asyncio.sleep(1)
    print('----end foo')


async def bar():
    print('****start bar')
    await asyncio.sleep(2)
    print('****end bar')


async def main():
    res = await asyncio.gather(foo(), bar())
    print(res)


if __name__ == '__main__':
    from os import path

    pass
