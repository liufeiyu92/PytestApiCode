"""
@File    : Application.py
@Time    : 2019/8/29 14:55
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
# 主机地址
# jtgl - 交通管理
# lysh - 商户管理
_HOST = {
    "jtgl_host": "http://192.168.10.232:30050",
    "lysh_host": "http://192.168.10.25:8804/lysh",

}
# 测试环境接口地址
_API_URL_DEBUG = {
    # ========================================================
    # 商户模块
    "api_user_list": "/um/user/list",
    "api_user_company_list": "/um/company/list",
    "api_user_department_list": "/um/department/findByCompanyId",
    "api_user_role_id": "/um/role/findByUserId",
    "api_user_add": "/um/userRole/add",
    "api_user_logout": "/um/user/logout",
    "api_role_list": "/um/role/list",
    "api_merchant_info_add": "/merchant/save",
    "api_merchant_info_update": "/merchant/update",
    "api_merchant_info_select": "/merchant/select?id=",
    "api_merchant_info_delete": "/merchant/delete?id=",
    "api_integrity_select": "/merchant-integrity/list",
    "api_complaint_select": "/merchant-complaint-record/list",
    "api_inspect_report": "/inspect/report",
    "api_inspect_list": "/inspect/list",
    "api_inspect_select_id": "/inspect/select?id=",
    "api_inspect_warn_list": "/inspect/warn/list",
    "api_inspect_rectification_list": "/inspect/rectification/list",
    "api_inspect_complete_list": "/inspect/complete/list",
    "api_inspect_rectification": "/inspect/warn/rectification",
    # ========================================================
    # 交通管理模块
    "api_menu_select": "/um/menu/show",
    "api_menu_select_by_id": "/um/menu/findByUserId",
    "api_device_select": "/device",
    "api_road_add": "/road",
    "api_road_update": "/road",
    "api_road_delete": "/road",
    "api_road_select": "/road/page",
    "api_road_relation": "/road/relation",
    "api_road_important": "/road/important",
    "api_monitor_add": "/lastMile",
    "api_monitor_update": "/lastMile",
    "api_monitor_delete": "/lastMile",
    "api_monitor_select": "/lastMile/page",
    "api_monitor_relation": "/lastMile/relation",
    "api_monitor_home": "/lastMile/home?id=",
    "api_msg_add": "/msg/user",
    "api_msg_update": "/msg/user",
    "api_msg_delete": "/msg/user",
    "api_msg_select": "/msg/user",
    "api_msg_param_update": "/param",
    "api_msg_param_get": "/param/template",
    "api_alarm_add": "/traffic/alarm",
    "api_alarm_ignore": "/traffic/alarm",
    "api_send_msg": "/send/message",
    "api_send_msg_list": "/send/message/page",
    "api_send_msg_list_alarm": "/send/message/list",
    "api_send_msg_detail": "/send/message/detail",
    "api_industry_province": "/industry/province",
    "api_industry_city": "/industry/city",
    "api_area_points": "/common/area/points",
    "api_screen_list": "/induction/screen",
    "api_car_source_statistics": "/car/source/statistics",
    "api_car_source_top_ten": "/car/source/top/ten",
    "api_flow_object": "/flow/object",
    "api_flow_statistics": "/flow/statistics",
    "api_flow_real_time": "/flow/realTime",
    "api_traffic_statics_report": "/traffic/report",
    "api_traffic_statics_trend": "/traffic/trend",
    "api_traffic_statics_info_sub": "/traffic/road/real/time/info/sub",
    "api_traffic_statics_road_info": "/traffic/road/real/time/info",
    "api_traffic_statics_road_info_top5": "/traffic/road/real/time/info/top5",
    "api_traffic_statics_jam_top5": "/traffic/road/jam/time/top5",
    "api_traffic_statics_jam_ratio": "/traffic/road/real/time/jam/ratio",
    "api_traffic_statics_list": "/traffic/list",
    "api_user_log_page_select": "/log/page",
    "api_user_log_user_select": "/log/user",
    "api_user_log_model_select": "/log/model",
    # ========================================================
    # 信息发布模块
    "api_xxfb_area_add": "/area/add",
    "api_xxfb_area_update": "/area/update",
    "api_xxfb_area_delete": "/area/delete",
    "api_xxfb_area_select": "/area/list",
    "api_xxfb_device_add": "/device/add",
    "api_xxfb_device_update": "/device/update",
    "api_xxfb_device_delete": "/device/delete",
    "api_xxfb_device_select": "/device/list",
    "api_xxfb_interface_add": "/interface/add",
    "api_xxfb_interface_update": "/interface/update",
    "api_xxfb_interface_delete": "/interface/delete",
    "api_xxfb_interface_select": "/interface/list",
    # ========================================================
    # 旅游停车模块
    "api_lytc_parking_lot_add": "/rm/parking-lot/save",
    "api_lytc_parking_lot_delete": "/rm/parking-lot/delete",
    "api_lytc_parking_lot_update": "/rm/parking-lot/update",
    "api_lytc_parking_lot_select": "/rm/parking-lot/get",
    "api_lytc_parking_lot_select_list": "/rm/parking-lot/list",
    "api_lytc_parking_lot_select_top_five": "/rm/parking-lot/full-record/list",
    "api_lytc_parking_realtime_select_list": "/rm/parking-space-realtime/list",
    "api_lytc_parking_summary": "/rm/parking-space-realtime/summary/list",
    "api_lytc_parking_space_select_all": "/rm/parking-space-realtime/scenic-area-parking-lot-saturation/get",
    "api_lytc_scenic_area_parking_prediction": "/rm/scenicAreaParkingSpacePrediction/scenicRealTimeAndPrediction",
    # ========================================================
    # 行业监测模块
    "api_lyjc_tourism_resource_add": "/tourismResource/add",
    "api_lyjc_tourism_resource_update": "/tourismResource/update",
    "api_lyjc_tourism_resource_select_by_id": "/tourismResource/findById",
    "api_lyjc_tourism_resource_delete_by_id": "/tourismResource/delete",
    "api_lyjc_tourism_resource_select_list": "/tourismResource/list",
    "api_lyjc_tourism_resource_select_area_list": "/tourismResource/areaList",
    "api_lyjc_tourism_resource_select_type_list": "/tourismResource/resourceTypeList"
}

# 接口正确响应代码
_API_RESPONSE_CODE = {
    "jtgl": "00000000",
    "lysh": "06010000",
    "xxfb": "04000000",
    "lytc": "02010000",
    "lyjc": "08010000"
}

# allure接口测试等级(重要程度) blocker > critical > normal > minor > trivial
_API_LEVEL = {
    1: "trivial",
    2: "minor",
    3: "normal",
    4: "critical",
    5: "blocker"
}

# jira项目数据字典
_JIRA_PROJECT_DATA = {
    "PM": "11000",
    "jtgl": "10801",
    "HYDD": "10800",
    "LYYT": "10805",
    "JCPT": "10809",
    "LYJC": "10803",
    "LYDSJ": "10802",
    "xxfb": "10806",
    "lytc": "10804",
    "lysh": "10807",
    "LYYX": "10808",
    "HYGL": "10900",
    "JZW": "10300",
    "FXYH": "10901",
    "TKTHJJCXT": "10700",
    "LEARN": "10600"
}

# 问题单名字转换
_ISSUE_ASSIGNEE_DATA = {
    "华桥": "陈华桥",
    "wangsong": "王淞",
    "liuyindog": "刘银",
    "sunriseXGY": "徐戈洋",
    "sunrisexgy": "徐戈洋"
}

# 问题单图标标题名字转换
_ISSUE_PLB_DATA = {
    "LYJTGL": "<洪雅智慧旅游 - 交通管理平台>"
}

# 图片名字转换
_ISSUE_PLB_NAME = {
    "LYJTGL": "[洪雅智慧旅游][交通管理平台]"
}

# assignee
# liuyindog - 刘银
# wangsong  - 王淞
# lijiafeng - 李家奉
# zbk       - 张斌可
_JIRA_ASSIGNEE_DATA = {
    "jtgl": "wangsong",  # 王淞
    "lysh": "lijiafeng",  # 李家奉
    "xxfb": "zbk",  # 张斌可
    "lytc": "lijiafeng",  # 李家奉
    "lyjc": "lijiafeng"  # 李家奉
}


def get_code(module_name):
    """根据模块名称获取接口响应码"""
    return _API_RESPONSE_CODE.get(module_name)


def get_project_id(module_name):
    """根据模块名称获取jira项目ID"""
    return _JIRA_PROJECT_DATA.get(module_name)


def get_assignee(module_name):
    """根据模块名称获取问题单经办人"""
    return _JIRA_ASSIGNEE_DATA.get(module_name)


def get_issue_assignee(old_name):
    """返回处理过的名字"""
    return _ISSUE_ASSIGNEE_DATA.get(old_name)


def get_plb_title_name(project_name):
    """获取问题单图标标题文字"""
    return _ISSUE_PLB_DATA.get(project_name)


def get_save_picture_name(project_name):
    """获取问题单图片保存名字"""
    return _ISSUE_PLB_NAME.get(project_name)


if __name__ == "__main__":
    # print(_get_code('jtgl'))
    pass
