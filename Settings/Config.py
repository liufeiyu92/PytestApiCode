"""
@File    : Config.py
@Time    : 2019/7/16 15:44
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import os
from Settings.Application import _API_URL_DEBUG, _HOST
from configparser import ConfigParser
from Common import LogCof
from Common.CreateTime import *


class Config:
    # ==========================================================================
    # titles:
    TITLE_DEBUG = "private_debug"
    TITLE_RELEASE = "online_release"
    TITLE_EMAIL = "mail"
    TITLE_DATABASE_66 = "database66"
    TITLE_DATABASE_181 = "database181"
    TITLE_DATABASE_184 = "database184"
    TITLE_DATABASE_201 = "database201"
    TITLE_JENKINS = "local_jenkins"
    TITLE_JIRA = "company_jira"

    # ==========================================================================
    # values:
    # database
    VALUE_DB_HOST = "connect_url"
    VALUE_DB_USER = "username"
    VALUE_DB_PASSWORD = "password"
    VALUE_DB_DEFAULT = "default_database"
    VALUE_DB_LYSH_DEFAULT = "hy_lysh_default_database"
    VALUE_DB_JTGL_DEFAULT = "hy_jtgl_default_database"
    VALUE_DB_HYDD_DEFAULT = "hy_hydd_default_database"
    VALUE_DB_LYTC_DEFAULT = "hy_lytc_default_database"
    VALUE_DB_XXFB_DEFAULT = "hy_xxfb_default_database"
    VALUE_DB_LYJC_DEFAULT = "hy_lyjc_default_database"

    # ==========================================================================
    # jira
    VALUE_JIRA_URL = "connect_url"
    VALUE_JIRA_USERNAME = "username"
    VALUE_JIRA_PASSWORD = "password"
    VALUE_JIRA_ISSUE_SWITCH = "switch"

    # ==========================================================================
    # jenkins
    VALUE_JENKINS_URL = "connect_url"
    VALUE_JENKINS_USER_NAME = "username"
    VALUE_JENKINS_PASSWORD = "password"
    VALUE_JENKINS_API_PASSWORD = "api_password"
    VALUE_JENKINS_ALLURE_REPORT_URL = "allure_report_url"

    # ==========================================================================
    # [debug/release]
    VALUE_TESTER = "tester"
    VALUE_ENVIRONMENT = "environment"
    VALUE_VERSION_CODE = "versionCode"
    VALUE_JTGL_HOST = "jtgl_host"
    VALUE_SH_HOST = "lysh_host"
    VALUE_XXFB_HOST = "xxfb_host"
    VALUE_LYTC_HOST = "lytc_host"
    VALUE_LYJC_HOST = "lyjc_host"
    VALUE_LOGIN_HOST = "loginHost"
    VALUE_LOGIN_INFO = "loginInfo"
    VALUE_TOKEN = "token"

    # ==========================================================================
    # 旅游资源模块-行业监测
    VALUE_LYJC_TOURISM_RES_ADD = "api_lyjc_tourism_resource_add"
    VALUE_LYJC_TOURISM_RES_UPDATE = "api_lyjc_tourism_resource_update"
    VALUE_LYJC_TOURISM_RES_SELECT_ID = "api_lyjc_tourism_resource_select_by_id"
    VALUE_LYJC_TOURISM_RES_DELETE = "api_lyjc_tourism_resource_delete_by_id"
    VALUE_LYJC_TOURISM_RES_SELECT_LIST = "api_lyjc_tourism_resource_select_list"
    VALUE_LYJC_TOURISM_RES_AREA_LIST = "api_lyjc_tourism_resource_select_area_list"
    VALUE_LYJC_TOURISM_RES_TYPE_LIST = "api_lyjc_tourism_resource_select_type_list"

    # ==========================================================================
    # 停车场基础信息-停车管理
    VALUE_LYTC_PARKING_LOT_ADD = "api_lytc_parking_lot_add"
    VALUE_LYTC_PARKING_LOT_DELETE = "api_lytc_parking_lot_delete"
    VALUE_LYTC_PARKING_LOT_UPDATE = "api_lytc_parking_lot_update"
    VALUE_LYTC_PARKING_LOT_SELECT_GET = "api_lytc_parking_lot_select"
    VALUE_LYTC_PARKING_LOT_SELECT_LIST = "api_lytc_parking_lot_select_list"
    VALUE_LYTC_PARKING_LOT_SELECT_TOP_FIVE = "api_lytc_parking_lot_select_top_five"
    # 车位饱和度实时信息-停车管理
    VALUE_LYTC_PARKING_REALTIME_SELECT_LIST = "api_lytc_parking_realtime_select_list"
    VALUE_LYTC_PARKING_SUMMARY = "api_lytc_parking_summary"
    VALUE_LYTC_PARKING_SELECT_ALL = "api_lytc_parking_space_select_all"
    # 景区停车预测模块-停车管理
    VALUE_LYTC_SCENIC_AREA_PREDICTION = "api_lytc_scenic_area_parking_prediction"

    # ==========================================================================
    # 区域管理模块-信息发布
    VALUE_XXFB_AREA_ADD = "api_xxfb_area_add"
    VALUE_XXFB_AREA_UPDATE = "api_xxfb_area_update"
    VALUE_XXFB_AREA_DELETE = "api_xxfb_area_delete"
    VALUE_XXFB_AREA_SELECT = "api_xxfb_area_select"
    # 设备管理模块-信息发布
    VALUE_XXFB_DEVICE_ADD = "api_xxfb_device_add"
    VALUE_XXFB_DEVICE_UPDATE = "api_xxfb_device_update"
    VALUE_XXFB_DEVICE_DELETE = "api_xxfb_device_delete"
    VALUE_XXFB_DEVICE_SELECT = "api_xxfb_device_select"
    # 接口管理模块-信息发布
    VALUE_XXFB_INTERFACE_ADD = "api_xxfb_interface_add"
    VALUE_XXFB_INTERFACE_UPDATE = "api_xxfb_interface_update"
    VALUE_XXFB_INTERFACE_DELETE = "api_xxfb_interface_delete"
    VALUE_XXFB_INTERFACE_SELECT = "api_xxfb_interface_select"

    # ==========================================================================
    # 用户模块-交通管理
    VALUE_API_USER_LIST = "api_user_list"
    VALUE_API_USER_COMPANY_LIST = "api_user_company_list"
    VALUE_API_USER_DEPARTMENT_LIST = "api_user_department_list"
    VALUE_API_USER_ROLE_ID = "api_user_role_id"
    VALUE_API_USER_ADD = "api_user_add"
    VALUE_API_USER_LOGOUT = "api_user_logout"
    # 菜单模块-交通管理
    VALUE_API_MENU_SELECT = "api_menu_select"
    VALUE_API_SELECT_ID = "api_menu_select_by_id"
    # 角色模块-交通管理
    VALUE_API_ROLE_LIST = "api_role_list"
    # 设备资源-交通管理
    VALUE_API_DEVICE_SELECT = "api_device_select"  # 分页获取设备信息
    # 路段资源-交通管理
    VALUE_API_ROAD_ADD = "api_road_add"  # 新增路段
    VALUE_API_ROAD_UPDATE = "api_road_update"  # 修改路段
    VALUE_API_ROAD_DELETE = "api_road_delete"  # 删除路段
    VALUE_API_ROAD_SELECT = "api_road_select"  # 分页查看路段
    VALUE_API_ROAD_RELATION = "api_road_relation"  # 获取关联对象信息
    VALUE_API_ROAD_IMPORTANT = "api_road_important"  # 设置路段为重点关注路段
    # 监控点信息-交通管理
    VALUE_API_MONITOR_ADD = "api_monitor_add"  # 新增监测点
    VALUE_API_MONITOR_UPDATE = "api_monitor_update"  # 修改监测点
    VALUE_API_MONITOR_DELETE = "api_monitor_delete"  # 删除监测点
    VALUE_API_MONITOR_SELECT = "api_monitor_select"  # 查看监控点
    VALUE_API_MONITOR_RELATION = "api_monitor_relation"  # 获取关联区域
    VALUE_API_MONITOR_HOME = "api_monitor_home"  # 获取首页监控点数据
    # 短信联系人-交通管理
    VALUE_API_MSG_ADD = "api_msg_add"  # 新增短信联系人
    VALUE_API_MSG_UPDATE = "api_msg_update"  # 修改短信联系人
    VALUE_API_MSG_DELETE = "api_msg_delete"  # 删除短信联系人
    VALUE_API_MSG_SELECT = "api_msg_select"  # 查看短信联系人
    # 短信配置-交通管理
    VALUE_API_MSG_PARAM_UPDATE = "api_msg_param_update"  # 修改短信配置
    VALUE_API_MSG_PARAM_GET = "api_msg_param_get"  # 获取电信配置
    # 交通告警-交通管理
    VALUE_API_ALARM_ADD = "api_alarm_add"  # 添加交通告警
    VALUE_API_ALARM_IGNORE = "api_alarm_ignore"  # 忽略交通告警
    # 发送短信-交通管理
    VALUE_API_SEND_MSG = "api_send_msg"  # 添加发送短信
    VALUE_API_SEND_MSG_LIST = "api_send_msg_list"  # 分页获取短信列表
    VALUE_API_SEND_MSG_ALARM = "api_send_msg_list_alarm"  # 获取类型用户列表和短信内容
    VALUE_API_SEND_MSG_DETAIL = "api_send_msg_detail"  # 获取短信详情
    # 产业运行监测-交通管理
    VALUE_API_INDUSTRY_PRO = "api_industry_province"  # 获取所有省份
    VALUE_API_INDUSTRY_CITY = "api_industry_city"  # 获取所有城市
    # 区域坐标点-交通管理
    VALUE_API_AREA_POINTS = "api_area_points"  # 获取区域坐标点
    # 诱导屏管理-交通管理
    VALUE_API_SCREEN_LIST = "api_screen_list"  # 获取诱导屏数据
    # 车来源管理-交通管理
    VALUE_API_CAR_SOURCE_STATISTICS = "api_car_source_statistics"  # 获取车来源统计数据
    VALUE_API_CAR_SOURCE_TOP_TEN = "api_car_source_top_ten"  # 导出车来源TOP10数据
    # 车流量管理-交通管理
    VALUE_API_CAR_FLOW_OBJ = "api_flow_object"  # 获取统计对象
    VALUE_API_CAR_FLOW_STATISTICS = "api_flow_statistics"  # 获取车流量统计数据
    VALUE_API_CAR_FLOW_REAL_TIME = "api_flow_real_time"  # 获取车流量实时数据
    # 交通统计-交通管理
    VALUE_API_TRAFFIC_STATICS_REPORT = "api_traffic_statics_report"  # 获取交通报表统计数据
    VALUE_API_TRAFFIC_STATICS_TREND = "api_traffic_statics_trend"  # 获取交通趋势分析数据
    VALUE_API_TRAFFIC_STATICS_INFO_SUB = "api_traffic_statics_info_sub"  # 旅游干道拥堵指数
    VALUE_API_TRAFFIC_STATICS_ROAD_INFO = "api_traffic_statics_road_info"  # 实时路况详情
    VALUE_API_TRAFFIC_STATICS_ROAD_TOP_FIVE = "api_traffic_statics_road_info_top5"  # 路段实时拥堵指数top5
    VALUE_API_TRAFFIC_STATICS_JAM_TOP_FIVE = "api_traffic_statics_jam_top5"  # 路段当日拥堵时长top5
    VALUE_API_TRAFFIC_STATICS_JAM_RATIO = "api_traffic_statics_jam_ratio"  # 拥堵里程汇总
    VALUE_API_TRAFFIC_STATICS_LIST = "api_traffic_statics_list"  # 获取实时路况详情筛选条件
    # 用户操作记录-交通管理
    VALUE_API_USER_LOG_PAGE_SELECT = "api_user_log_page_select"  # 分页查询用户操作记录
    VALUE_API_USER_LOG_USER_SELECT = "api_user_log_user_select"  # 查询系统所有用户
    VALUE_API_USER_LOG_MODEL_SELECT = "api_user_log_model_select"  # 查询系统所有模块

    # ==========================================================================
    # 商户模块-商户系统
    VALUE_API_SH_ADD = "api_merchant_info_add"  # 新增商户
    VALUE_API_SH_UPDATE = "api_merchant_info_update"  # 修改商户
    VALUE_API_SH_SELECT = "api_merchant_info_select"  # 查看商户
    VALUE_API_SH_DELETE = "api_merchant_info_delete"  # 删除商户
    # 商户诚信模块-商户系统
    VALUE_API_INTEGRITY_SELECT = "api_integrity_select"  # 查询商户诚信列表
    # 商户投诉服务模块-商户系统
    VALUE_API_COMPLAINT_SELECT = "api_complaint_select"  # 查询商户投诉列表
    # 巡查管理+经营预警-商户系统
    VALUE_API_INSPECT_REPORT = "api_inspect_report"  # 巡检上报
    VALUE_API_INSPECT_LIST = "api_inspect_list"  # 巡查管理列表
    VALUE_API_INSPECT_SELECT_ID = "api_inspect_select_id"  # 根据ID查询巡查
    VALUE_API_INSPECT_WARN_LIST = "api_inspect_warn_list"  # 经营预警列表下发
    VALUE_API_INSPECT_RECTIFICATION_LIST = "api_inspect_rectification_list"  # 经营预警列表整改
    VALUE_API_INSPECT_COMPLETE_LIST = "api_inspect_complete_list"  # 经营预警列表完成
    VALUE_API_INSPECT_RECTIFICATION = "api_inspect_rectification"  # 整改

    # ==========================================================================
    # [mail]
    VALUE_SMTP_SERVER = "smtpserver"
    VALUE_SENDER = "sender"
    VALUE_RECEIVER = "receiver"
    VALUE_USERNAME = "username"
    VALUE_PASSWORD = "password"

    # ==========================================================================
    # path
    path_dir = str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

    def __init__(self):
        """
        初始化
        """
        self.config = ConfigParser()
        self.pytest_config = ConfigParser()
        self.log = LogCof.MyLog()
        self.conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'Config.ini')
        self.pytest_config_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
                                               'pytest.ini')
        self.xml_report_path = Config.path_dir + '/Report/xml/' + HandleTime.timestamp_to_string_report(time.time())
        self.html_report_path = Config.path_dir + '/Report/html/' + HandleTime.timestamp_to_string_report(time.time())

        if not os.path.exists(self.conf_path) or not os.path.exists(self.pytest_config_path):
            raise FileNotFoundError("请确保配置文件存在！")

        self.config.read(self.conf_path, encoding='utf-8')
        self.pytest_config.read(self.pytest_config_path, encoding='utf-8')

        # ==========================================================================
        # jira
        self.jira_url = self.get_conf(Config.TITLE_JIRA, Config.VALUE_JIRA_URL)
        self.jira_username = self.get_conf(Config.TITLE_JIRA, Config.VALUE_JIRA_USERNAME)
        self.jira_password = self.get_conf(Config.TITLE_JIRA, Config.VALUE_JIRA_PASSWORD)
        self.jira_issue_switch = self.get_conf(Config.TITLE_JIRA, Config.VALUE_JIRA_ISSUE_SWITCH)

        # ==========================================================================
        # jenkins
        self.jenkins_url = self.get_conf(Config.TITLE_JENKINS, Config.VALUE_JENKINS_URL)
        self.jenkins_username = self.get_conf(Config.TITLE_JENKINS, Config.VALUE_JENKINS_USER_NAME)
        self.jenkins_password = self.get_conf(Config.TITLE_JENKINS, Config.VALUE_JENKINS_PASSWORD)
        self.jenkins_api_password = self.get_conf(Config.TITLE_JENKINS, Config.VALUE_JENKINS_API_PASSWORD)
        self.jenkins_allure_report_url = self.get_conf(Config.TITLE_JENKINS, Config.VALUE_JENKINS_ALLURE_REPORT_URL)

        # ==========================================================================
        # database-66
        self.connect_url_db_66 = self.get_conf(Config.TITLE_DATABASE_66, Config.VALUE_DB_HOST)
        self.username_db_66 = self.get_conf(Config.TITLE_DATABASE_66, Config.VALUE_DB_USER)
        self.password_db_66 = self.get_conf(Config.TITLE_DATABASE_66, Config.VALUE_DB_PASSWORD)
        self.default_db_66 = self.get_conf(Config.TITLE_DATABASE_66, Config.VALUE_DB_DEFAULT)

        # ==========================================================================
        # database-181
        self.connect_url_db_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_HOST)
        self.username_db_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_USER)
        self.password_db_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_PASSWORD)
        self.default_db_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_DEFAULT)
        self.hy_lysh_default_database_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_LYSH_DEFAULT)
        self.hy_jtgl_default_database_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_JTGL_DEFAULT)
        self.hy_hydd_default_database_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_HYDD_DEFAULT)
        self.hy_lytc_default_database_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_LYTC_DEFAULT)
        self.hy_xxfb_default_database_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_XXFB_DEFAULT)
        self.hy_lyjc_default_database_181 = self.get_conf(Config.TITLE_DATABASE_181, Config.VALUE_DB_LYJC_DEFAULT)

        # ==========================================================================
        # database-184
        self.connect_url_db_184 = self.get_conf(Config.TITLE_DATABASE_184, Config.VALUE_DB_HOST)
        self.username_db_184 = self.get_conf(Config.TITLE_DATABASE_184, Config.VALUE_DB_USER)
        self.password_db_184 = self.get_conf(Config.TITLE_DATABASE_184, Config.VALUE_DB_PASSWORD)
        self.default_db_184 = self.get_conf(Config.TITLE_DATABASE_184, Config.VALUE_DB_DEFAULT)

        # ==========================================================================
        # database-201
        self.connect_url_db_201 = self.get_conf(Config.TITLE_DATABASE_201, Config.VALUE_DB_HOST)
        self.username_db_201 = self.get_conf(Config.TITLE_DATABASE_201, Config.VALUE_DB_USER)
        self.password_db_201 = self.get_conf(Config.TITLE_DATABASE_201, Config.VALUE_DB_PASSWORD)
        self.default_db_201 = self.get_conf(Config.TITLE_DATABASE_201, Config.VALUE_DB_DEFAULT)

        # ==========================================================================
        # host-debug
        self.tester_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_TESTER)
        self.environment_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_ENVIRONMENT)
        self.versionCode_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_VERSION_CODE)
        self.host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_JTGL_HOST)
        self.merchant_host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_SH_HOST)
        self.xxfb_host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_XXFB_HOST)
        self.lytc_host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_LYTC_HOST)
        self.lyjc_host_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_LYJC_HOST)
        self.loginHost_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_LOGIN_HOST)
        self.loginInfo_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_LOGIN_INFO)
        self.token_debug = self.get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # ==========================================================================
        # debug-tourism-resource-lyjc
        self.lyjc_tourism_res_add_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_ADD)
        self.lyjc_tourism_res_update_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_UPDATE)
        self.lyjc_tourism_res_select_id_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_SELECT_ID)
        self.lyjc_tourism_res_delete_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_DELETE)
        self.lyjc_tourism_res_select_list_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_SELECT_LIST)
        self.lyjc_tourism_res_area_list_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_AREA_LIST)
        self.lyjc_tourism_res_type_list_debug = _API_URL_DEBUG.get(Config.VALUE_LYJC_TOURISM_RES_TYPE_LIST)

        # ==========================================================================
        # debug-parking-lot-lytc
        self.lytc_parking_lot_add_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_ADD)
        self.lytc_parking_lot_delete_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_DELETE)
        self.lytc_parking_lot_update_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_UPDATE)
        self.lytc_parking_lot_select_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_SELECT_GET)
        self.lytc_parking_lot_select_list_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_SELECT_LIST)
        self.lytc_parking_lot_select_top_five_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_LOT_SELECT_TOP_FIVE)
        # debug-parking-realtime-lytc
        self.lytc_parking_realtime_select_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_REALTIME_SELECT_LIST)
        self.lytc_parking_summary_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_SUMMARY)
        self.lytc_parking_select_all_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_PARKING_SELECT_ALL)
        # debug-scenic-area-prediction-lytc
        self.lytc_scenic_area_prediction_debug = _API_URL_DEBUG.get(Config.VALUE_LYTC_SCENIC_AREA_PREDICTION)

        # ==========================================================================
        # debug-area-xxfb
        self.xxfb_area_add_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_AREA_ADD)
        self.xxfb_area_update_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_AREA_UPDATE)
        self.xxfb_area_delete_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_AREA_DELETE)
        self.xxfb_area_select_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_AREA_SELECT)
        # debug-device-xxfb
        self.xxfb_device_add_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_DEVICE_ADD)
        self.xxfb_device_update_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_DEVICE_UPDATE)
        self.xxfb_device_delete_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_DEVICE_DELETE)
        self.xxfb_device_select_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_DEVICE_SELECT)
        # debug-interface-xxfb
        self.xxfb_interface_add_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_INTERFACE_ADD)
        self.xxfb_interface_update_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_INTERFACE_UPDATE)
        self.xxfb_interface_delete_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_INTERFACE_DELETE)
        self.xxfb_interface_select_debug = _API_URL_DEBUG.get(Config.VALUE_XXFB_INTERFACE_SELECT)

        # ==========================================================================
        # debug-user-jtgl
        self.user_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LIST)
        self.user_company_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_COMPANY_LIST)
        self.user_department_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_DEPARTMENT_LIST)
        self.user_role_id_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_ROLE_ID)
        self.user_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_ADD)
        self.user_logout_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LOGOUT)
        # debug-menu-jtgl
        self.menu_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_MENU_SELECT)
        self.menu_select_by_id_debug = _API_URL_DEBUG.get(Config.VALUE_API_SELECT_ID)
        # debug-role-jtgl
        self.role_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROLE_LIST)
        # debug-road-jtgl
        self.road_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_ADD)
        self.road_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_UPDATE)
        self.road_delete_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_DELETE)
        self.road_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_SELECT)
        self.road_relation_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_RELATION)
        self.road_important_debug = _API_URL_DEBUG.get(Config.VALUE_API_ROAD_IMPORTANT)
        # debug-device-jtgl
        self.device_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_DEVICE_SELECT)
        # debug-monitor-jtgl
        self.monitor_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_ADD)
        self.monitor_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_UPDATE)
        self.monitor_delete_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_DELETE)
        self.monitor_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_SELECT)
        self.monitor_relation_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_RELATION)
        self.monitor_home_debug = _API_URL_DEBUG.get(Config.VALUE_API_MONITOR_HOME)
        # debug-msg-jtgl
        self.msg_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_ADD)
        self.msg_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_UPDATE)
        self.msg_delete_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_DELETE)
        self.msg_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_SELECT)
        # debug-msg-param-jtgl
        self.msg_param_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_PARAM_UPDATE)
        self.msg_param_get_debug = _API_URL_DEBUG.get(Config.VALUE_API_MSG_PARAM_GET)
        # debug-alarm-jtgl
        self.alarm_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_ALARM_ADD)
        self.alarm_ignore_debug = _API_URL_DEBUG.get(Config.VALUE_API_ALARM_IGNORE)
        # debug-send-msg-jtgl
        self.send_msg_debug = _API_URL_DEBUG.get(Config.VALUE_API_SEND_MSG)
        self.send_msg_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_SEND_MSG_LIST)
        self.send_msg_alarm_debug = _API_URL_DEBUG.get(Config.VALUE_API_SEND_MSG_ALARM)
        self.send_msg_detail_debug = _API_URL_DEBUG.get(Config.VALUE_API_SEND_MSG_DETAIL)
        # debug-industry-jtgl
        self.industry_pro_debug = _API_URL_DEBUG.get(Config.VALUE_API_INDUSTRY_PRO)
        self.industry_city_debug = _API_URL_DEBUG.get(Config.VALUE_API_INDUSTRY_CITY)
        # debug-area-points-jtgl
        self.area_points_debug = _API_URL_DEBUG.get(Config.VALUE_API_AREA_POINTS)
        # debug-screen-list-jtgl
        self.screen_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_SCREEN_LIST)
        # debug-car-source-jtgl
        self.car_source_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_SOURCE_STATISTICS)
        self.car_source_top_ten_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_SOURCE_TOP_TEN)
        # debug-car-flow-jtgl
        self.car_flow_obj_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_FLOW_OBJ)
        self.car_flow_statistics_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_FLOW_STATISTICS)
        self.car_flow_real_time_debug = _API_URL_DEBUG.get(Config.VALUE_API_CAR_FLOW_REAL_TIME)
        # debug-traffic-statics-jtgl
        self.traffic_statics_report_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_REPORT)
        self.traffic_statics_trend_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_TREND)
        self.traffic_statics_info_sub_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_INFO_SUB)
        self.traffic_statics_road_info_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_ROAD_INFO)
        self.traffic_statics_road_top5_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_ROAD_TOP_FIVE)
        self.traffic_statics_jam_top5_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_JAM_TOP_FIVE)
        self.traffic_statics_jam_ratio_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_JAM_RATIO)
        self.traffic_statics_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_TRAFFIC_STATICS_LIST)
        # debug-user-log-jtgl
        self.user_log_page_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LOG_PAGE_SELECT)
        self.user_log_user_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LOG_USER_SELECT)
        self.user_log_model_debug = _API_URL_DEBUG.get(Config.VALUE_API_USER_LOG_MODEL_SELECT)

        # ==========================================================================
        # debug-merchant-lysh
        self.merchant_add_debug = _API_URL_DEBUG.get(Config.VALUE_API_SH_ADD)
        self.merchant_update_debug = _API_URL_DEBUG.get(Config.VALUE_API_SH_UPDATE)
        self.merchant_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_SH_SELECT)
        self.merchant_delete_debug = _API_URL_DEBUG.get(Config.VALUE_API_SH_DELETE)
        # debug-integrity-lysh
        self.integrity_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_INTEGRITY_SELECT)
        # debug-complaint-lysh
        self.complaint_select_debug = _API_URL_DEBUG.get(Config.VALUE_API_COMPLAINT_SELECT)
        # debug-inspect-lysh
        self.inspect_report_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_REPORT)
        self.inspect_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_LIST)
        self.inspect_select_id_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_SELECT_ID)
        self.inspect_warn_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_WARN_LIST)
        self.inspect_rec_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_RECTIFICATION_LIST)
        self.inspect_complete_list_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_COMPLETE_LIST)
        self.inspect_rectification_debug = _API_URL_DEBUG.get(Config.VALUE_API_INSPECT_RECTIFICATION)

        # ==========================================================================
        # release
        self.tester_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_TESTER)
        self.environment_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_ENVIRONMENT)
        self.versionCode_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_VERSION_CODE)
        self.host_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_JTGL_HOST)
        self.loginHost_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_LOGIN_HOST)
        self.loginInfo_release = self.get_conf(Config.TITLE_RELEASE, Config.VALUE_LOGIN_INFO)

        # ==========================================================================
        # email
        self.smtpserver = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_SMTP_SERVER)
        self.sender = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_SENDER)
        self.receiver = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_RECEIVER)
        self.username = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_USERNAME)
        self.password = self.get_conf(Config.TITLE_EMAIL, Config.VALUE_PASSWORD)

    def get_conf(self, title, value):
        """
        配置文件读取
        :param title:
        :param value:
        :return:
        """
        return self.config.get(title, value)

    def get_pytest_cof(self, title, value):
        """
        pytest配置获取
        :param title:
        :param value:
        :return:
        """
        return self.pytest_config.get(title, value)

    def set_conf(self, title, value, text):
        """
        配置文件修改
        :param title:
        :param value:
        :param text:
        :return:
        """
        self.config.set(title, value, text)
        with open(self.conf_path, "w+", encoding="utf-8") as f:
            return self.config.write(f)

    def add_conf(self, title):
        """
        配置文件添加
        :param title:
        :return:
        """
        self.config.add_section(title)
        with open(self.conf_path, "w+") as f:
            return self.config.write(f)


if __name__ == "__main__":
    # print(_API_URL_DEBUG.get(Config.VALUE_API_USER_LIST))
    pass
