"""
@File    : test_api_hy_lysh_complaint.py
@Time    : 2019/8/20 16:16
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_lysh import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestComplaint:
    """
    商户模块-商户投诉管理服务
    """
    api_complaint_url = (
        'http://192.168.10.200/hy/lysh/hy-lysh-merchant-manage/blob/master/doc/api-merchant-complaint-record.md')

    @allure.testcase(f'{api_complaint_url}#查询商户投诉列表', '接口文档地址')
    @allure.feature('<商户模块>商户投诉管理服务')
    @allure.story('查询商户投诉列表')
    @pytest.mark.Complaint
    def test_lysh_complaint_01(self, error_handling):
        """
        描述：查询商户投诉列表
        请求路径：/merchant-complaint-record/list
        请求方式：POST
        """
        # 准备issue报告数据
        method_name = "商户投诉管理服务-查询商户投诉列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().merchant_host_debug, Config().complaint_select_debug)
        data = {
            "current": 1,
            "size": 10,
            "condition": {
                "merchantId": 3
            }
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
