"""
@File    : test_api_hy_lysh_merchant.py
@Time    : 2019/8/16 13:56
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_lysh import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestMerchant:
    """
    商户模块-商户信息
    """
    api_role_text_url = 'http://192.168.10.200/hy/lysh/hy-lysh-merchant-manage/blob/master/doc/api-merchant-info.md'
    _merchant_para = [(random.randint(1, 999), str(158) + str(random.randint(10000000, 99999999))),
                      (random.randint(1, 999), str(158) + str(random.randint(10000000, 99999999))),
                      (random.randint(1, 999), str(158) + str(random.randint(10000000, 99999999))),
                      (random.randint(1, 999), str(158) + str(random.randint(10000000, 99999999))),
                      (random.randint(1, 999), str(158) + str(random.randint(10000000, 99999999)))]

    @pytest.fixture(params=_merchant_para)
    def _merchant_para(self, request):
        """
        test_lysh_merchant_01用例参数列表
        """
        return request.param

    @allure.testcase(f'{api_role_text_url}', '接口文档地址')
    @allure.feature('<商户模块>商户信息')
    @allure.story('商户信息-增删改查')
    @pytest.mark.skip("商户信息不能增删改-2019.11.5")
    @pytest.mark.Merchant
    def test_lysh_merchant_01(self, _merchant_para, error_handling):
        """
        描述：商户信息-增删改查-正向流程
        请求路径/请求方式:
            · 新增 POST /merchant/save
            · 修改 POST /merchant/update
            · 查询 GET /merchant/select?id={商户id}
            · 删除 GET /merchant/delete?id={商户id}
        """
        # 准备issue报告数据
        method_name = "商户信息-商户信息增删改查"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 新增用户-请求地址和请求参数
        add_url = "%s%s" % (Config().merchant_host_debug, Config().merchant_add_debug)
        add_data = {
            "merchantName": f"宜宾燃面{_merchant_para[0]}",
            "regionalCode": "001008",
            "regionalName": "这是一个区域",
            "scenicCode": "001009",
            "scenicName": "景区名称",
            "merchantTypeCode": "1001010",
            "merchantTypeName": "商户甲",
            "businessScopeName": "这是经营范围",
            "businessAddress": "天府之国",
            "corporate": "刘飞宇",
            "cardId": "511502199009021951",
            "contact": "刘飞宇",
            "phoneNumber": f"{_merchant_para[1]}",
            "licenseRegisterTime": "2019-08-06 10:10:10",
            "licenseExpireTime": "2025-08-06 10:10:10",
            "licensePhoto": "http://172.0.0.1/xx/xx,http://172.0.0.1/xx/xx",
            "storePhoto": "http://172.0.0.1/xx/xx"
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_keys(add_result, 'body', 'success')
        expect_response_add = get_code(module_name)
        expect_success_add = True
        merchant_id = GetKey.get_keys(add_result, 'body', 'data')

        # 断言
        assume(response_add == expect_response_add)
        assume(success_add == expect_success_add)

        # 查看用户-请求地址和请求参数
        select_url = "%s%s%d" % (Config().merchant_host_debug, Config().merchant_select_debug, merchant_id)

        # 发起请求
        select_result = Request.get_request(url=select_url, headers={"token": token})

        # 获取返回值
        response_select = GetKey.get_keys(select_result, 'body', 'code')
        success_select = GetKey.get_keys(select_result, 'body', 'success')
        expect_response_select = get_code(module_name)
        expect_success_select = True
        expect_merchant_name = GetKey.get_keys(select_result, 'body', 'merchantName')
        expect_phone_num = GetKey.get_keys(select_result, 'body', 'phoneNumber')

        # 断言
        assume(response_select == expect_response_select)
        assume(success_select == expect_success_select)
        assume(f"宜宾燃面{_merchant_para[0]}" == expect_merchant_name)
        assume(_merchant_para[1] == expect_phone_num)

        # 修改用户-请求地址和请求参数
        update_url = "%s%s" % (Config().merchant_host_debug, Config().merchant_update_debug)
        update_data = {
            "id": merchant_id,
            "merchantName": f"巧面馆{_merchant_para[0]}",
            "regionalCode": "001008",
            "regionalName": "这是一个区域",
            "scenicCode": "001009",
            "scenicName": "景区名称",
            "merchantTypeCode": "1001010",
            "merchantTypeName": "商户甲",
            "businessScopeName": "这是经营范围",
            "businessAddress": "天府之国",
            "corporate": "刘飞宇",
            "cardId": "511502199009021951",
            "contact": "刘飞宇",
            "phoneNumber": f"13588889999",
            "licenseRegisterTime": "2019-08-06 10:10:10",
            "licenseExpireTime": "2025-08-06 10:10:10",
            "licensePhoto": "http://172.0.0.1/xx/xx,http://172.0.0.1/xx/xx",
            "storePhoto": "http://172.0.0.1/xx/xx"
        }

        # 发起请求
        update_result = Request.post_request(url=update_url, json=update_data, headers={"token": token})

        # 获取返回值
        response_update = GetKey.get_keys(update_result, 'body', 'code')
        success_update = GetKey.get_keys(update_result, 'body', 'success')
        expect_response_update = get_code(module_name)
        expect_success_update = True

        # 查看商户-发起请求
        select_result2 = Request.get_request(url=select_url, headers={"token": token})

        # 获取返回值
        response_select_2 = GetKey.get_keys(select_result2, 'body', 'code')
        success_select_2 = GetKey.get_keys(select_result2, 'body', 'success')
        expect_response_select_2 = get_code(module_name)
        expect_success_select_2 = True
        merchant_name_update = GetKey.get_keys(select_result2, 'body', 'merchantName')
        phone_num_update = GetKey.get_keys(select_result2, 'body', 'phoneNumber')
        expect_merchant_name_update = f"巧面馆{_merchant_para[0]}"
        expect_merchant_phone_num_update = "13588889999"

        # 删除商户-发起请求
        delete_url = "%s%s%d" % (Config().merchant_host_debug, Config().merchant_delete_debug, merchant_id)

        # 发起请求
        delete_result = Request.get_request(url=delete_url, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_keys(delete_result, 'body', 'success')
        expect_response_delete = get_code(module_name)
        expect_success_delete = True

        # 查看商户-发起请求
        select_result3 = Request.get_request(url=select_url, headers={"token": token})

        # 获取返回值
        response_select_3 = GetKey.get_keys(select_result3, 'body', 'code')
        success_select_3 = GetKey.get_keys(select_result3, 'body', 'success')
        data_select_3 = GetKey.get_keys(select_result3, 'body', 'data')
        expect_response_select_3 = get_code(module_name)
        expect_success_select_3 = True
        expect_data_select_3 = None

        # 报告执行步骤
        with allure.step('第一步：新增商户'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(token, 'token')
            allure.attach(f'code码返回值为：{expect_response_add}\n'
                          f'success返回值为：{expect_success_add}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步：查看商户'):
            allure.attach(select_url, '请求地址')
            allure.attach(format_cn_res(select_result['body']), '响应内容')
            allure.attach(token, 'token')
            allure.attach(f'code码返回值为：{expect_response_select}\n'
                          f'success返回值为：{expect_success_select}\n'
                          f'商户名称返回值为：宜宾燃面{_merchant_para[0]}\n'
                          f'电话返回值为：{_merchant_para[1]}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select}\n'
                          f'success返回值为：{success_select}\n'
                          f'商户名称返回值为：{expect_merchant_name}\n'
                          f'电话返回值为：{expect_phone_num}',
                          '实际结果')
        with allure.step('第三部：修改商户-并查看该商户修改数据'):
            allure.attach(update_url, '请求地址')
            allure.attach(format_cn_res(update_data), '请求参数')
            allure.attach(format_cn_res(update_result['body']), '响应内容')
            allure.attach(token, 'token')
            allure.attach(f'code码返回值为：{expect_response_select_2}\n'
                          f'success返回值为：{expect_success_select_2}\n'
                          f'商户名称返回值为：{expect_merchant_name_update}\n'
                          f'电话返回值为：{expect_merchant_phone_num_update}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select_2}\n'
                          f'success返回值为：{success_select_2}\n'
                          f'商户名称返回值为：{merchant_name_update}\n'
                          f'电话返回值为：{phone_num_update}',
                          '实际结果')
        with allure.step('第四部：删除商户-并查看是否删除成功'):
            allure.attach(delete_url, '请求地址')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(token, 'token')
            allure.attach(f'code码返回值为：{expect_response_select_3}\n'
                          f'success返回值为：{expect_success_select_3}\n'
                          f'删除后查询商户数据返回值为：{expect_data_select_3}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select_3}\n'
                          f'success返回值为：{success_select_3}\n'
                          f'删除后查询商户数据返回值为：{data_select_3}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_update == expect_response_update)
        assert_2 = assume(success_update == expect_success_update)
        # 断言
        assert_3 = assume(response_select_2 == expect_response_select_2)
        assert_4 = assume(success_select_2 == expect_success_select_2)
        assert_5 = assume(merchant_name_update == expect_merchant_name_update)
        assert_6 = assume(phone_num_update == expect_merchant_phone_num_update)
        # 断言
        assert_7 = assume(response_delete == expect_response_delete)
        assert_8 = assume(success_delete == expect_success_delete)
        # 断言
        assert_9 = assume(response_select_3 == expect_response_select_3)
        assert_10 = assume(success_select_3 == expect_success_select_3)
        assert_11 = assume(data_select_3 == expect_data_select_3)

        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
                and assert_5
                and assert_6
                and assert_7
                and assert_8
                and assert_9
                and assert_10
                and assert_11
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
