"""
@File    : test_api_hy_lysh_inspect.py
@Time    : 2019/8/20 16:57
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_lysh import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestInspect:
    """
    商户模块-巡查管理+经营预警
    """
    api_inspect_url = (
        'http://192.168.10.200/hy/lysh/hy-lysh-merchant-manage/blob/master/doc/api-merchant-inspect.md')

    @allure.testcase(f'{api_inspect_url}#巡检上报', '接口文档地址')
    @allure.feature('<商户模块>巡查管理+经营预警')
    @allure.story('巡检上报')
    @pytest.mark.Inspect
    def test_lysh_inspect_01(self, error_handling):
        """
        描述：巡检上报
        请求路径：/inspect/report
        请求方式：POST
        """
        # 准备issue报告数据
        method_name = "巡查管理+经营预警-巡检上报"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().merchant_host_debug, Config().inspect_report_debug)
        data = {
            "merchantId": 3,
            "inspectId": 123,
            "inspectName": "刘飞宇",
            "inspectTime": "2019-08-06 10:10:10",
            "inspectStatus": 0,
            "inspectPhoto": "http://127.0.0.1",
            "problemTypeCode": "00088",
            "problemTypeName": "无照经营",
            "problemName": "很脏,很乱,很差",
            "problemDesc": "到处都是苍蝇,很垃圾"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_inspect_url}#巡查管理列表', '接口文档地址')
    @allure.feature('<商户模块>巡查管理+经营预警')
    @allure.story('巡查管理列表')
    @pytest.mark.Inspect
    def test_lysh_inspect_list_01(self, error_handling):
        """
        描述：巡检上报-查询全部
        请求路径：/inspect/list
        请求方式：POST
        """
        # 准备issue报告数据
        method_name = "巡查管理+经营预警-巡查管理列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().merchant_host_debug, Config().inspect_list_debug)
        data = {
            "current": 1,
            "size": 10
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_inspect_url}#巡查管理列表', '接口文档地址')
    @allure.feature('<商户模块>巡查管理+经营预警')
    @allure.story('巡查管理列表')
    @pytest.mark.Inspect
    def test_lysh_inspect_list_02(self, db_181, error_handling):
        """
        描述：巡检上报-查询指定列表
        请求路径：/inspect/list
        请求方式：POST
        """
        # 准备issue报告数据
        method_name = "巡查管理+经营预警-巡查管理列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT inspect_name FROM hy_inspect_manage WHERE `status` = 0"""
        sql_result = read_sql(sql=sql, db_obj=db_181)
        inspector = random.choice(sql_result)[0]

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().merchant_host_debug, Config().inspect_list_debug)
        data = {
            "current": 1,
            "size": 10,
            "condition": {
                "inspector": inspector
            }
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_inspect_url}#根据ID查询巡查', '接口文档地址')
    @allure.feature('<商户模块>巡查管理+经营预警')
    @allure.story('根据ID查询巡查')
    @pytest.mark.Inspect
    def test_lysh_inspect_by_id_01(self, db_181, error_handling):
        """
        描述：巡检上报-根据ID查询巡查
        请求路径：/inspect/select?id={inspectID}
        请求方式：GET
        """
        method_name = "巡查管理+经营预警-根据ID查询巡查"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT `id` FROM hy_inspect_manage"""
        sql_result = read_sql(sql=sql, db_obj=db_181)
        inspect_id = random.choice(sql_result)[0]

        # 接口请求地址和请求数据
        url = "%s%s%s" % (Config().merchant_host_debug, Config().inspect_select_id_debug, inspect_id)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_inspect_url}#经营预警列表下发', '接口文档地址')
    @allure.feature('<商户模块>巡查管理+经营预警')
    @allure.story('经营预警列表下发')
    @pytest.mark.Inspect
    def test_lysh_inspect_warn_01(self, error_handling):
        """
        描述：经营预警列表下发
        请求路径：/inspect/warn/list
        请求方式：POST
        """
        method_name = "巡查管理+经营预警-经营预警列表下发"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().merchant_host_debug, Config().inspect_warn_list_debug)
        data = {
            "current": 1,
            "size": 10
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        inspect_status = GetKey.get_keys(result['body'], 'records', 'inspectStatus')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(inspect_status, int):
            inspect_status = (0 != inspect_status)
        elif isinstance(inspect_status, list):
            inspect_status = (0 not in inspect_status)
        else:
            inspect_status = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        assert_3 = assume(inspect_status == True)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_inspect_url}#经营预警列表整改', '接口文档地址')
    @allure.feature('<商户模块>巡查管理+经营预警')
    @allure.story('经营预警列表整改')
    @pytest.mark.Inspect
    def test_lysh_inspect_rec_01(self, error_handling):
        """
        描述：经营预警列表-查询
        请求路径：/inspect/rectification/list
        请求方式：POST
        """
        method_name = "巡查管理+经营预警-经营预警列表整改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().merchant_host_debug, Config().inspect_rec_list_debug)
        data = {
            "current": 1,
            "size": 10
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_inspect_url}#经营预警列表完成', '接口文档地址')
    @allure.feature('<商户模块>巡查管理+经营预警')
    @allure.story('经营预警列表完成')
    @pytest.mark.Inspect
    def test_lysh_inspect_complete_01(self, error_handling):
        """
        描述：经营预警列表-查询
        请求路径：/inspect/complete/list
        请求方式：POST
        """
        method_name = "巡查管理+经营预警-经营预警列表完成"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().merchant_host_debug, Config().inspect_complete_list_debug)
        data = {
            "current": 1,
            "size": 10
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_inspect_url}#整改', '接口文档地址')
    @allure.feature('<商户模块>巡查管理+经营预警')
    @allure.story('整改')
    @pytest.mark.Inspect
    def test_lysh_inspect_complete_01(self, db_181, error_handling):
        """
        描述：整改
        请求路径：/inspect/warn/rectification
        请求方式：POST
        """
        method_name = "巡查管理+经营预警-整改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT `id` FROM hy_inspect_manage"""
        sql_result = read_sql(sql=sql, db_obj=db_181)
        inspect_id = random.choice(sql_result)[0]

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().merchant_host_debug, Config().inspect_rectification_debug)
        data = {'id': inspect_id}
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
