"""
@File    : test_api_hy_jtgl_msg.py
@Time    : 2019/9/4 9:47
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestMsg:
    """
    交通管理-短信联系人
    """
    api_msg_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/api-short-msg-user.md'

    @allure.testcase(f'{api_msg_text_url}#新增联系人', '接口文档地址-新增联系人')
    @allure.testcase(f'{api_msg_text_url}#修改联系人', '接口文档地址-修改联系人')
    @allure.testcase(f'{api_msg_text_url}#获取联系人列表', '接口文档地址-获取联系人列表')
    @allure.testcase(f'{api_msg_text_url}#删除联系人', '接口文档地址-删除联系人')
    @allure.feature('<交通管理>短信联系人')
    @allure.story('短信联系人-增删查改')
    @pytest.mark.Msg
    def test_msg_01(self, error_handling):
        """
        描述：新增联系人-修改联系人-查看联系人是否修改成功-删除联系人
        请求路径:
            · 新增联系人 POST /msg/user
            · 修改联系人 PUT /msg/user
            · 查看联系人 GET /msg/user
            · 删除联系人 DELETE /msg/user/{id}
        """
        # 准备issue报告数据
        method_name = "短信联系人-短信联系人增删查改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 新增-接口请求地址和请求数据
        add_url = "%s%s" % (Config().host_debug, Config().msg_add_debug)
        add_data = {
            "userName": "Patrick",
            "autoSendMsg": 1,
            "phoneNumber": 15808319823
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_key(add_result, 'success')
        msg_id = GetKey.get_key(add_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 修改-接口请求地址和请求数据
        update_url = "%s%s" % (Config().host_debug, Config().msg_update_debug)
        update_data = {
            "id": msg_id,
            "userName": "Tom",
            "autoSendMsg": 1,
            "phoneNumber": 17608280656,
            "deleteStatus": 1
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        update_result = Request.put_request(url=update_url, json=update_data, headers={"token": token})

        # 获取返回值
        response_update = GetKey.get_keys(update_result, 'body', 'code')
        success_update = GetKey.get_key(update_result, 'success')
        msg_name_update = "Tom"
        msg_phone_update = "17608280656"

        # 查看-接口其你去地址和请求数据
        select_url = "%s%s" % (Config().host_debug, Config().msg_select_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        select_result = Request.get_request(url=select_url, headers={"token": token})

        # 获取返回值
        select_id = GetKey.get_keys(select_result, 'data', 'id')
        select_index = select_id.index(msg_id)
        data_list = select_result['body'].get('data')
        new_select_data = data_list[select_index]
        msg_name_select = GetKey.get_key(new_select_data, 'userName')
        msg_phone_select = GetKey.get_key(new_select_data, 'phoneNumber')
        response_select = GetKey.get_keys(select_result, 'body', 'code')
        success_select = GetKey.get_key(select_result, 'success')

        # 删除-接口请求地址和请求数据
        delete_url = "%s%s/%s" % (Config().host_debug, Config().msg_delete_debug, msg_id)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        delete_result = Request.delete_request(url=delete_url, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_key(delete_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-新增联系人'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步-修改联系人'):
            allure.attach(update_url, '请求地址')
            allure.attach(format_cn_res(update_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(update_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_update}\n'
                          f'success返回值为：{success_update}',
                          '实际结果')
        with allure.step('第三步-查看联系人'):
            allure.attach(select_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(select_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}\n'
                          f'msg_name返回值为{msg_name_update}\n'
                          f'phone返回值为{msg_phone_update}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select}\n'
                          f'success返回值为：{success_select}\n'
                          f'msg_name返回值为{msg_name_select}\n'
                          f'phone返回值为{msg_phone_update}',
                          '实际结果')
        with allure.step('第四步-删除联系人'):
            allure.attach(delete_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_delete}\n'
                          f'success返回值为：{success_delete}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_add == expect_response)
        assert_2 = assume(success_add == expect_success)
        assert_3 = assume(response_select == expect_response)
        assert_4 = assume(success_select == expect_success)
        assert_5 = assume(msg_name_select == msg_name_update)
        assert_6 = assume(msg_phone_select == msg_phone_update)
        assert_7 = assume(response_update == expect_response)
        assert_8 = assume(success_update == expect_success)
        assert_9 = assume(response_delete == expect_response)
        assert_10 = assume(success_delete == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
                and assert_5
                and assert_6
                and assert_7
                and assert_8
                and assert_9
                and assert_10
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
