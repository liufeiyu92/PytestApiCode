"""
@File    : test_api_hy_jtgl_road.py
@Time    : 2019/8/26 13:49
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestRoad:
    """
    交通管理-路段资源模块
    """
    api_road_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/roadSection.md'

    @allure.testcase(f'{api_road_text_url}#新增路段', '接口文档地址-新增路段')
    @allure.testcase(f'{api_road_text_url}#修改路段', '接口文档地址-修改路段')
    @allure.testcase(f'{api_road_text_url}#删除路段', '接口文档地址-删除路段')
    @allure.feature('<交通管理>路段资源模块')
    @allure.story('路段-增删改')
    @pytest.mark.skip('路段设置为常量,不能增删改-2019.10.1')
    @pytest.mark.Road
    def test_road_01(self, error_handling):
        """
        描述：新增路段-修改路段-删除路段
        请求路径:
            · 新增路段 POST /road
            · 修改路段 PUT /road
            · 删除路段 DELETE /road/{id}
        """
        # 准备issue报告数据
        method_name = "路段资源模块-路段增删改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 新增-接口请求地址和请求数据
        add_url = "%s%s" % (Config().host_debug, Config().road_add_debug)
        add_data = {
            "roadSectionName": "测试路段",
            "roadSectionDirection": 1,
            "relationObjectCodes": ["005004", "005005"],
            "mileageCount": 5,
            "roadSectionDescribe": "测试路段-刘飞宇"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_key(add_result, 'success')
        road_id = GetKey.get_key(add_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 修改-接口请求地址和请求数据
        update_url = "%s%s" % (Config().host_debug, Config().road_update_debug)
        update_data = {
            "id": road_id,
            "roadSectionName": "测试路段-更新",
            "roadSectionDirection": 1,
            "relationObjectCodes": ["005001", "005002"],
            "mileageCount": 5,
            "roadSectionDescribe": "测试路段-刘飞宇-更新"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        update_result = Request.put_request(url=update_url, json=update_data, headers={"token": token})

        # 获取返回值
        response_update = GetKey.get_keys(update_result, 'body', 'code')
        success_update = GetKey.get_key(update_result, 'success')

        # 删除-接口请求地址和请求数据
        delete_url = "%s%s/%s" % (Config().host_debug, Config().road_delete_debug, road_id)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        delete_result = Request.delete_request(url=delete_url, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_key(delete_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-新增路段'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步-修改路段'):
            allure.attach(update_url, '请求地址')
            allure.attach(format_cn_res(update_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(update_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_update}\n'
                          f'success返回值为：{success_update}',
                          '实际结果')
        with allure.step('第三步-删除路段'):
            allure.attach(delete_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_delete}\n'
                          f'success返回值为：{success_delete}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_add == expect_response)
        assert_2 = assume(success_add == expect_success)
        assert_3 = assume(response_update == expect_response)
        assert_4 = assume(success_update == expect_success)
        assert_5 = assume(response_delete == expect_response)
        assert_6 = assume(success_delete == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
                and assert_5
                and assert_6
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_road_text_url}#分页查询路段', '接口文档地址')
    @allure.feature('<交通管理>路段资源模块')
    @allure.story('分页查询路段')
    @pytest.mark.Road
    def test_road_select_01(self, error_handling):
        """
        描述：分页查询路段
        请求路径: /road/page
        请求方式: POST
        """
        # 准备issue报告数据
        method_name = "路段资源模块-分页查询路段"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().road_select_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)
        data = {"page": 1,
                "size": 10}

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_road_text_url}#获取关联对象信息', '接口文档地址')
    @allure.feature('<交通管理>路段资源模块')
    @allure.story('获取关联对象信息')
    @pytest.mark.Road
    def test_road_relation_01(self, error_handling):
        """
        描述：获取关联对象信息
        请求路径: /road/relation
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "路段资源模块-获取关联对象信息"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().road_relation_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_road_text_url}#设置路段为重点关注路段', '接口文档地址')
    @allure.feature('<交通管理>路段资源模块')
    @allure.story('设置路段为重点关注路段')
    @pytest.mark.Road
    def test_road_important_01(self, error_handling, db_181):
        """
        描述：设置路段为重点关注路段
        请求路径: /road/important
        请求方式: PUT
        """
        # 准备issue报告数据
        method_name = "路段资源模块-设置路段为重点关注路段"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT `id` FROM hy_road_section"""
        sql_result = read_sql(sql=sql, db_obj=db_181)
        road_id = random.choice(sql_result)[0]

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().road_important_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)
        data = {
            "roadSectionIds": [road_id]
        }

        # 发起请求
        result = Request.put_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
