"""
@File    : test_api_hy_jtgl_user.py
@Time    : 2019/7/18 15:08
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestUser:
    """
    交通管理-用户信息模块
    """
    api_user_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/user.md'
    _user_01_para = PandasHelper(module_name=data_module_name,
                                 file_name='user-user_01.xlsx').get_excel_param_data()
    _user_02_para = PandasHelper(module_name=data_module_name,
                                 file_name='user-user_02.xlsx').get_excel_param_data()

    @pytest.fixture(params=_user_01_para)
    def _user_param(self, request):
        """
        test_user_01用例参数列表
        """
        return request.param

    @pytest.fixture(params=_user_02_para)
    def _user_param_2(self, request):
        """
        test_user_02用例参数列表
        """
        return request.param

    @pytest.mark.User
    @allure.testcase(f'{api_user_text_url}#用户列表', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('用户列表')
    def test_user_01(self, _user_param, error_handling):
        """
        描述：查看用户列表正向流程-根据page来分页查询用户
        请求路径：/um/user/list
        请求方式：Post
        """
        # 准备issue报告数据
        method_name = "用户信息模块-用户列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_list_debug)
        data = {"page": _user_param[0],
                "size": _user_param[1]}
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        page_num = GetKey.get_key(result, 'pageNum')
        expect_response = get_code(module_name)
        expect_page_num = _user_param[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'page返回值为：{expect_page_num}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'page返回值为：{page_num}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(page_num == expect_page_num)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

        if _user_param[2] == 1:
            # 存入用户名字
            user_name = GetKey.get_keys(result, 'list', 'userName')
            if user_name is not None:
                for i in user_name:
                    Consts.API_USER_NAME_LIST.append(i)

            # 存入公司ID
            company_id = GetKey.get_keys(result, 'company', 'id')
            if company_id is not None:
                for i in set(company_id):
                    Consts.API_COMPANY_ID_LIST.append(i)

    @pytest.mark.User
    @allure.testcase(f'{api_user_text_url}#用户列表', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('用户列表')
    def test_user_02(self, _user_param_2, error_handling):
        """
        描述：查看用户列表反向流程-page和size选择其一或者全部都不填
        请求路径：/um/user/list
        请求方式：Post
        """
        # 准备issue报告数据
        method_name = "用户信息模块-用户列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_list_debug)
        data = {"page": _user_param_2[0],
                "size": _user_param_2[1]}
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = '05009999'
        expect_success = False

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @pytest.mark.User
    @allure.testcase(f'{api_user_text_url}#用户列表', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('用户列表')
    def test_user_03(self, error_handling):
        """
        描述：查看用户列表-条件查询-根据用户名查询
        请求路径：/um/user/list
        请求方式：Post
        """
        # 准备issue报告数据
        method_name = "用户信息模块-用户列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备用户名
        username = random.choice(Consts.API_USER_NAME_LIST)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_list_debug)
        data = {"page": 1,
                "size": 10,
                "userName": username}
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        user_name = GetKey.get_keys(result, 'body', 'userName')
        expect_response = get_code(module_name)
        expect_username = username

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'username返回值为：{expect_username}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'username返回值为：{user_name}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(user_name == expect_username)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @pytest.mark.User
    @allure.testcase(f'{api_user_text_url}#用户列表', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('用户列表')
    def test_user_04(self, error_handling):
        """
        描述：查看用户列表-条件查询-根据单位id查询
        请求路径：/um/user/list
        请求方式：Post
        """
        # 准备issue报告数据
        method_name = "用户信息模块-用户列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 单位id
        company_id = random.choice(Consts.API_COMPANY_ID_LIST)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_list_debug)
        data = {"page": 1,
                "size": 10,
                "companyId": company_id}
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        company_res_id = GetKey.get_keys(result, 'company', 'id')
        expect_response = get_code(module_name)
        company_res_id = company_res_id if isinstance(company_res_id, list) == True else [company_res_id]
        expect_res_company_id = list(set(company_res_id))
        expect_company_id = expect_res_company_id[-1]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'单位id返回值为：{company_id}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'单位id返回值为：{expect_company_id}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(company_id == expect_company_id)
        # 判断返回值里所有单位id去重后是否长度为1(可确定是否查出的单位只有1个)
        assert_3 = assume(len(expect_res_company_id) == 1)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_user_text_url}#单位列表', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('单位列表')
    @pytest.mark.User
    def test_user_company_01(self, error_handling):
        """
        描述：查看单位列表
        请求路径: /um/company/list
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "用户信息模块-单位列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_company_list_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_user_text_url}#部门列表', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('部门列表')
    @pytest.mark.User
    def test_user_department_01(self, error_handling):
        """
        描述：查看部门列表
        请求路径: /um/department/findByCompanyId/{company_id}
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "用户信息模块-部门列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        id_len = len(Consts.API_COMPANY_ID_LIST)
        assume(id_len > 0)
        random_num = random.randint(0, id_len - 1)
        url = "%s%s/%s" % (Config().host_debug, Config().user_department_list_debug,
                           Consts.API_COMPANY_ID_LIST[random_num])
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(str(set(Consts.API_COMPANY_ID_LIST)), '单位ID')
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_user_text_url}#用户对应角色列表', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('用户对应角色列表')
    @pytest.mark.User
    def test_user_role_01(self, db_181_hydd, error_handling):
        """
        描述：用户对应角色列表-根据用户ID查询
        请求路径: /um/role/findByUserId/{user_id}
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "用户信息模块-用户对应角色列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT `id` FROM t_user"""
        sql_result = read_sql(sql=sql, db_obj=db_181_hydd)
        user_id = random.choice(sql_result)[0]

        url = "%s%s/%s" % (Config().host_debug, Config().user_role_id_debug, user_id)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(str(user_id), '用户ID')
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_user_text_url}#用户分配角色', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('用户分配角色')
    @pytest.mark.User
    def test_user_role_add_01(self, error_handling):
        """
        描述：用户分配角色
        请求路径: /um/userRole/add
        请求方式: POST
        """
        pass

    @allure.testcase(f'{api_user_text_url}#退出登录', '接口文档地址')
    @allure.feature('<交通管理>用户信息模块')
    @allure.story('退出登录')
    @pytest.mark.User
    def test_user_logout_01(self, jira_obj, error_handling):
        """
        描述：退出登录
        请求路径: /um/user/logout
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "用户信息模块-退出登录"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_logout_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
