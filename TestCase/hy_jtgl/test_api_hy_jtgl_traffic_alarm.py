"""
@File    : test_api_hy_jtgl_traffic_alarm.py
@Time    : 2019/9/4 14:40
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestAlarm:
    """
    交通管理-交通告警
    """
    api_alarm_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/trafficAlarm.md'

    @allure.testcase(f'{api_alarm_text_url}#添加交通告警', '接口文档地址-添加交通告警')
    @allure.testcase(f'{api_alarm_text_url}#忽略交通告警', '接口文档地址-忽略交通告警')
    @allure.feature('<交通管理>交通告警')
    @allure.story('交通告警-添加删除')
    @pytest.mark.Alarm
    @pytest.mark.skip(reason="开发告知内部接口不用测试")
    def test_alarm_01(self, error_handling):
        """
        描述：添加交通告警-忽略交通告警
        请求路径:
            · 添加交通告警 POST /traffic/alarm
            · 忽略交通告警 PUT /traffic/alarm/{id}
        """
        # 准备issue报告数据
        method_name = "交通告警-添加删除"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 添加交通告警-接口请求地址和请求数据
        add_url = "%s%s" % (Config().host_debug, Config().alarm_add_debug)
        time_now = time.time()
        add_data = {
            "roadSectionName": "A0003路段",
            "mileageCount": 11,
            "roadSectionDescribe": "2",
            "alarmReason": "拥挤",
            "congestionIndex": "3.3",
            "congestionMileage": "4",
            "averageSpeed": "5",
            "alarmTime": HandleTime.timestamp_to_string(time_now),
            "alarmType": 1
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_key(add_result, 'success')
        alarm_id = GetKey.get_key(add_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 忽略交通告警-接口请求地址和请求数据
        delete_url = "%s%s/%s" % (Config().host_debug, Config().alarm_ignore_debug, alarm_id)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        delete_result = Request.put_request(url=delete_url, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_key(delete_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-添加交通告警'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步-忽略交通告警'):
            allure.attach(delete_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_delete}\n'
                          f'success返回值为：{success_delete}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_add == expect_response)
        assert_2 = assume(success_add == expect_success)
        assert_3 = assume(response_delete == expect_response)
        assert_4 = assume(success_delete == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
