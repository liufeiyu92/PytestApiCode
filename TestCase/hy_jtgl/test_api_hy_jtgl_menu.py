"""
@File    : test_api_hy_jtgl_menu.py
@Time    : 2019/8/22 17:28
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestMenu:
    """
    交通管理-菜单信息模块
    """
    api_menu_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/menu.md'

    @allure.testcase(f'{api_menu_text_url}#全部菜单树', '接口文档地址')
    @allure.feature('<交通管理>菜单信息模块')
    @allure.story('全部菜单树')
    @pytest.mark.Menu
    def test_menu_select_01(self, error_handling):
        """
        描述：全部菜单树
        请求路径: /um/menu/show
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "菜单信息模块-全部菜单树"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().menu_select_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_menu_text_url}#当前用户菜单树', '接口文档地址')
    @allure.feature('<交通管理>菜单信息模块')
    @allure.story('当前用户菜单树')
    @pytest.mark.Menu
    def test_menu_select_id_01(self, error_handling):
        """
        描述：当前用户菜单树
        请求路径: /um/menu/findByUserId
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "菜单信息模块-当前用户菜单树"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().menu_select_by_id_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
