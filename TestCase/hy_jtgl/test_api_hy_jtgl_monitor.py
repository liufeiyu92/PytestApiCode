"""
@File    : test_api_hy_jtgl_monitor.py
@Time    : 2019/8/30 9:24
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestMonitor:
    """
    交通管理-监控点信息
    """
    api_monitor_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/monitorSite.md'

    @allure.testcase(f'{api_monitor_text_url}#新增监控点', '接口文档地址-新增监控点')
    @allure.testcase(f'{api_monitor_text_url}#修改监控点', '接口文档地址-修改监控点')
    @allure.testcase(f'{api_monitor_text_url}#分页查询监控点', '接口文档地址-查询监控点')
    @allure.testcase(f'{api_monitor_text_url}#删除监控点', '接口文档地址-删除监控点')
    @allure.feature('<交通管理>监控点信息')
    @allure.story('监控点-增删查改')
    @pytest.mark.skip('监控点不测增删改-2019.11.5')
    @pytest.mark.Monitor
    def test_monitor_01(self, db_181, error_handling):
        """
        描述：新增监控点-修改监控点-查看监控点是否修改成功-删除监控点
        请求路径:
            · 新增监控点 POST /lastMile
            · 修改监控点 PUT /lastMile
            · 查看监控点 POST /lastMile/page
            · 删除监控点 DELETE /lastMile/{id}
        """
        # 准备issue报告数据
        method_name = "监控点信息-监控点增删查改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT `id` FROM hy_road_section WHERE delete_status = 1"""
        sql_result = read_sql(sql=sql, db_obj=db_181)
        road_id = random.choice(sql_result)[0]

        # 新增-接口请求地址和请求数据
        num = random.randint(10, 20)
        add_url = "%s%s" % (Config().host_debug, Config().monitor_add_debug)
        add_data = {
            "monitorSiteName": "测试监视设备",
            "deviceId": f"test{num}",
            "relationCode": "001002",
            "relationArea": "止戈镇",
            "detailedAddress": f"止戈镇地址{num}",
            "longitude": "120.159856",
            "latitude": "32.25544",
            "roadId": f"{road_id}"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_key(add_result, 'success')
        monitor_id = GetKey.get_key(add_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 修改-接口请求地址和请求数据
        update_url = "%s%s" % (Config().host_debug, Config().monitor_update_debug)
        update_data = {
            "id": monitor_id,
            "monitorSiteName": f"测试监视设备-更新-{monitor_id}",
            "deviceId": f"test{num}",
            "relationCode": "001002",
            "relationArea": "止戈镇",
            "detailedAddress": f"止戈镇地址{num}",
            "longitude": "120.159856",
            "latitude": "32.25544",
            "roadId": f"{road_id}"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        update_result = Request.put_request(url=update_url, json=update_data, headers={"token": token})

        # 获取返回值
        response_update = GetKey.get_keys(update_result, 'body', 'code')
        success_update = GetKey.get_key(update_result, 'success')
        monitor_name_update = f"测试监视设备-更新-{monitor_id}"

        # 查看-接口其你去地址和请求数据
        select_url = "%s%s" % (Config().host_debug, Config().monitor_select_debug)
        select_data = {
            "page": 1,
            "size": 10,
            "monitorSiteId": "",
            "monitorSiteName": f"测试监视设备-更新-{monitor_id}",
            "detailedAddress": ""
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        select_result = Request.post_request(url=select_url, json=select_data, headers={"token": token})

        # 获取返回值
        response_select = GetKey.get_keys(select_result, 'body', 'code')
        success_select = GetKey.get_key(select_result, 'success')
        monitor_name_select = GetKey.get_keys(select_result, 'data', 'monitorSiteName')

        # 删除-接口请求地址和请求数据
        delete_url = "%s%s/%s" % (Config().host_debug, Config().monitor_delete_debug, monitor_id)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        delete_result = Request.delete_request(url=delete_url, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_key(delete_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-新增监控点'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步-修改监控点'):
            allure.attach(update_url, '请求地址')
            allure.attach(format_cn_res(update_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(update_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_update}\n'
                          f'success返回值为：{success_update}',
                          '实际结果')
        with allure.step('第三步-查看监控点'):
            allure.attach(select_url, '请求地址')
            allure.attach(format_cn_res(select_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(select_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}\n'
                          f'monitor_name返回值为{monitor_name_update}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select}\n'
                          f'success返回值为：{success_select}\n'
                          f'monitor_name返回值为{monitor_name_select}',
                          '实际结果')
        with allure.step('第四步-删除监控点'):
            allure.attach(delete_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_delete}\n'
                          f'success返回值为：{success_delete}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_add == expect_response)
        assert_2 = assume(success_add == expect_success)
        assert_3 = assume(response_select == expect_response)
        assert_4 = assume(success_select == expect_success)
        assert_5 = assume(monitor_name_select == monitor_name_update)
        assert_6 = assume(response_update == expect_response)
        assert_7 = assume(success_update == expect_success)
        assert_8 = assume(response_delete == expect_response)
        assert_9 = assume(success_delete == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
                and assert_5
                and assert_6
                and assert_7
                and assert_8
                and assert_9
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_monitor_text_url}#获取关联区域', '接口文档地址')
    @allure.feature('<交通管理>监控点信息')
    @allure.story('获取关联区域')
    @pytest.mark.Monitor
    def test_monitor_relation_01(self, error_handling):
        """
        描述：获取关联区域
        请求路径: /lastMile/relation
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "监控点信息-获取关联区域"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().monitor_relation_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_monitor_text_url}#获取首页监控点数据', '接口文档地址')
    @allure.feature('<交通管理>监控点信息')
    @allure.story('获取首页监控点数据')
    @pytest.mark.Monitor
    def test_monitor_home_01(self, db_181, error_handling):
        """
        描述：获取首页监控点数据
        请求路径: /lastMile/home
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "监控点信息-获取首页监控点数据"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT `id` FROM hy_monitor_site WHERE delete_status = 1"""
        sql_result = read_sql(sql=sql, db_obj=db_181)
        monitor_id = random.choice(sql_result)[0]

        # 接口请求地址和请求数据
        url = "%s%s%d" % (Config().host_debug, Config().monitor_home_debug, monitor_id)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)
        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
