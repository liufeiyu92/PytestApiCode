"""
@File    : test_api_hy_jtgl_car_source.py
@Time    : 2019/9/5 15:22
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestCarSource:
    """
    交通管理-车来源管理
    """
    api_car_source_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/carSource.md'

    @allure.testcase(f'{api_car_source_text_url}#获取车来源统计数据', '接口文档地址')
    @allure.feature('<交通管理>车来源管理模块')
    @allure.story('获取车来源统计数据')
    @pytest.mark.CarSource
    def test_car_source_01(self, db_181, error_handling):
        """
        描述：获取车来源统计数据
        请求路径: /car/source/statistics
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "车来源管理模块-获取车来源统计数据"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT `id` FROM hy_monitor_site WHERE delete_status = 1"""
        sql_result = read_sql(sql=sql, db_obj=db_181)
        monitor_id = random.choice(sql_result)[0]
        # locationLevelEnum
        location_level_data = random.choice(["COUNTRY", "PROVINCE"])
        # statisticsGraininessEnum
        statistics_data = random.choice(["HOUR", "DAY", "MONTH"])
        # statisticsBeginTime
        start_time = int(str(time.time())[0:7] + "00000")
        # statisticsEndTime
        end_time = int(round(time.time() * 1000))

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().car_source_debug)
        data = {
            "statisticsObject": str(monitor_id),
            "statisticsBeginTime": start_time,
            "statisticsEndTime": end_time,
            "locationLevelEnum": location_level_data,
            "statisticsGraininessEnum": statistics_data
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, data=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_car_source_text_url}#导出车来源TOP10数据', '接口文档地址')
    @allure.feature('<交通管理>车来源管理模块')
    @allure.story('导出车来源TOP10数据')
    @pytest.mark.CarSource
    def test_car_source_top_ten_01(self, error_handling):
        """
        描述：导出车来源TOP10数据
        请求路径: /car/source/top/ten
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "车来源管理模块-导出车来源TOP10数据"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        # locationLevelEnum
        location_level_data = random.choice(["COUNTRY", "PROVINCE"])

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().car_source_top_ten_debug)
        data = {
            "locationLevelEnum": location_level_data

        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, data=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
