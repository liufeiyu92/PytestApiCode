"""
@File    : test_api_hy_jtgl_role.py
@Time    : 2019/7/31 16:56
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestRole:
    """
    交通管理-角色信息模块
    """
    api_role_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/role.md'
    _role_01_para = PandasHelper(module_name=data_module_name,
                                 file_name='role-role_01.xlsx').get_excel_param_data()

    @pytest.fixture(params=_role_01_para)
    def _role_01_param(self, request):
        """
        test_role_01用例参数列表
        """
        return request.param

    @allure.testcase(f'{api_role_text_url}#角色列表', '接口文档地址')
    @allure.feature('<交通管理>角色信息模块')
    @allure.story('角色列表')
    @pytest.mark.Role
    def test_role_01(self, _role_01_param, error_handling):
        """
        描述：角色列表查看-根据page来分页查询用户
        请求路径: /um/role/list
        请求方式: POST
        """
        # 准备issue报告数据
        method_name = "角色信息模块-角色列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().role_list_debug)
        data = {"page": _role_01_param[0],
                "size": _role_01_param[1]}
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        page_num = GetKey.get_key(result, 'pageNum')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_page_num = _role_01_param[0]
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'page返回值为：{expect_page_num}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'page返回值为：{page_num}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        if _role_01_param[2] == 1:
            # 存入角色ID
            user_role_id = GetKey.get_keys(result, 'list', 'id')
            if user_role_id is not None:
                if not isinstance(user_role_id, list):
                    Consts.API_ROLE_ID_LIST.append(user_role_id)
                else:
                    for i in user_role_id:
                        Consts.API_ROLE_ID_LIST.append(i)

            # 存入角色名称
            user_role_name = GetKey.get_keys(result, 'list', 'name')
            if user_role_name is not None:
                if not isinstance(user_role_name, list):
                    Consts.API_ROLE_NAME_LIST.append(user_role_name)
                else:
                    for i in user_role_name:
                        Consts.API_ROLE_NAME_LIST.append(i)

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(page_num == expect_page_num)
        assert_3 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_role_text_url}#角色列表', '接口文档地址')
    @allure.feature('<交通管理>角色信息模块')
    @allure.story('角色列表')
    @pytest.mark.Role
    def test_role_02(self, error_handling):
        """
        描述：角色列表查看-根据角色名查询
        请求路径: /um/role/list
        请求方式: POST
        """
        # 准备issue报告数据
        method_name = "角色信息模块-角色列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        role_name = random.choice(Consts.API_ROLE_NAME_LIST)
        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().role_list_debug)
        data = {"page": 1,
                "size": 10,
                "roleName": role_name}
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        role_res_name = GetKey.get_keys(result, 'body', 'name')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        expect_role_name = role_name

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'roleName返回值为：{expect_role_name}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'roleName返回值为：{role_res_name}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        assert_3 = assume(expect_role_name in role_res_name)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_role_text_url}#新增角色', '接口文档地址')
    @allure.feature('<交通管理>角色信息模块')
    @allure.story('新增角色')
    @pytest.mark.Role
    def test_role_add_01(self, error_handling):
        """
        描述：新增角色
        请求路径: /um/role/add
        请求方式: POST
        """
        pass
