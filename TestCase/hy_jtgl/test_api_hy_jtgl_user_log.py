"""
@File    : test_api_hy_jtgl_user_log.py
@Time    : 2019/9/7 10:49
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestUserLog:
    """
    交通管理-用户操作记录
    """
    api_user_log_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/userLog.md'

    @allure.testcase(f'{api_user_log_text_url}#分页查询用户操作记录', '接口文档地址')
    @allure.feature('<交通管理>用户操作记录')
    @allure.story('分页查询用户操作记录')
    @pytest.mark.UserLog
    def test_log_page_01(self, error_handling):
        """
        描述：分页查询用户操作记录
        请求路径: /log/page
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "用户操作记录-分页查询用户操作记录"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_log_page_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 获取最大page页数，再次请求
        total_pages = GetKey.get_keys(result['body'], 'data', 'totalPages')

        data = {
            "page": total_pages
        }

        # 发起请求
        sec_result = Request.get_request(url=url, data=data, headers={"token": token})

        # 获取返回值
        sec_response = GetKey.get_keys(sec_result, 'body', 'code')
        sec_success = GetKey.get_key(sec_result, 'success')
        if isinstance(sec_response, list):
            sec_response = sec_response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '默认page查看-响应内容')
            allure.attach(format_cn_res(sec_result['body']), '最大page查看-响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果-默认page查看')
            allure.attach(f'code码返回值为：{sec_response}\n'
                          f'success返回值为：{sec_success}',
                          '实际结果-最大page查看')
        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        assert_3 = assume(sec_response == expect_response)
        assert_4 = assume(sec_success == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_user_log_text_url}#查询系统所有用户', '接口文档地址')
    @allure.feature('<交通管理>用户操作记录')
    @allure.story('查询系统所有用户')
    @pytest.mark.UserLog
    def test_log_user_01(self, error_handling):
        """
        描述：查询系统所有用户
        请求路径: /log/user
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "用户操作记录-查询系统所有用户"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_log_user_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_user_log_text_url}#查询系统所有模块', '接口文档地址')
    @allure.feature('<交通管理>用户操作记录')
    @allure.story('查询系统所有模块')
    @pytest.mark.UserLog
    def test_log_model_01(self, error_handling):
        """
        描述：查询系统所有模块
        请求路径: /log/model
        请求方式: GET
        """
        # 准备issue报告数据
        method_name = "用户操作记录-查询系统所有模块"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().host_debug, Config().user_log_model_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_key(result, 'success')
        expect_response = get_code(module_name)
        expect_success = True
        if isinstance(response, list):
            response = response[0]

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
