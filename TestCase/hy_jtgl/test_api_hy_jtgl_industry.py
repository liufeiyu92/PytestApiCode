"""
@File    : test_api_hy_jtgl_industry.py
@Time    : 2019/9/5 10:09
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_jtgl import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestIndustry:
    """
    交通管理-产业运行监测
    """
    api_industry_text_url = 'http://192.168.10.200/hy/document/blob/master/hy-jtgl/api/industryRun.md'

    @allure.testcase(f'{api_industry_text_url}#获取所有省份', '接口文档地址-获取所有省份')
    @allure.testcase(f'{api_industry_text_url}#获取省份下的所有城市', '接口文档地址-获取省份下的所有城市')
    @allure.feature('<交通管理>产业运行监测')
    @allure.story('产业运行监测-获取省份-城市')
    @pytest.mark.Industry
    def test_industry_01(self, error_handling):
        """
        描述：获取所有省份-获取省份下的所有城市
        请求路径:
            · 获取所有省份 GET /industry/province
            · 获取省份下的所有城市 GET /industry/city
        """
        # 准备issue报告数据
        method_name = "产业运行监测-获取省份和城市"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 获取所有省份-接口请求地址和请求数据
        pro_url = "%s%s" % (Config().host_debug, Config().industry_pro_debug)

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        pro_result = Request.get_request(url=pro_url, headers={"token": token})

        # 获取返回值'
        response_pro = GetKey.get_keys(pro_result, 'body', 'code')
        success_pro = GetKey.get_key(pro_result, 'success')
        all_province = GetKey.get_key(pro_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 获取省份下的所有城市-接口请求地址和请求数据
        city_url = "%s%s" % (Config().host_debug, Config().industry_city_debug)
        city_data = {
            "provinceName": random.choice(all_province)
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        city_result = Request.get_request(url=city_url, data=city_data, headers={"token": token})

        # 获取返回值
        response_city = GetKey.get_keys(city_result, 'body', 'code')
        success_city = GetKey.get_key(city_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-获取所有省份'):
            allure.attach(pro_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(pro_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_pro}\n'
                          f'success返回值为：{success_pro}',
                          '实际结果')
        with allure.step('第二步-获取省份下的所有城市'):
            allure.attach(city_url, '请求地址')
            allure.attach(str(city_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(city_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_city}\n'
                          f'success返回值为：{success_city}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_pro == expect_response)
        assert_2 = assume(success_pro == expect_success)
        assert_3 = assume(response_city == expect_response)
        assert_4 = assume(success_city == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
