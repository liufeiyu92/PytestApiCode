"""
@File    : test_api_hy_lyjc_tourism_resource.py
@Time    : 2019/10/18 14:00
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_lyjc import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestTourismResource:
    """
    行业监测-旅游资源模块
    """
    api_text_url = (
        'http://192.168.10.200/hy/lyjc/hy-lyjc-industrial-operation-monitoring/blob/master/doc/tourism-resource.md')

    @allure.testcase(f'{api_text_url}#新增旅游资源', '接口文档地址-新增旅游资源')
    @allure.testcase(f'{api_text_url}#更新旅游资源', '接口文档地址-更新旅游资源')
    @allure.testcase(f'{api_text_url}#资源详情', '接口文档地址-资源详情')
    @allure.testcase(f'{api_text_url}#删除资源', '接口文档地址-删除资源')
    @allure.feature('<行业监测>旅游资源模块')
    @allure.story('旅游资源-增删查改')
    @pytest.mark.lyjcTourismRes
    def test_tourism_resource_01(self, error_handling):
        """
        描述：新增旅游资源-修改旅游资源-查看旅游资源是否修改成功-删除旅游资源
        请求路径:
            · 新增旅游资源 POST /tourismResource/add
            · 修改旅游资源 POST /tourismResource/update
            · 查看旅游资源 GET /tourismResource/findById/{ID}
            · 删除旅游资源 GET /tourismResource/delete/{ID}
        """
        # 准备issue报告数据
        method_name = "旅游资源模块-旅游资源增删查改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 新增-接口请求地址和请求数据
        add_url = "%s%s" % (Config().lyjc_host_debug, Config().lyjc_tourism_res_add_debug)
        add_data = {
            "name": "测试资源",
            "area": "001001",
            "location": "洪川镇",
            "type": "008001",
            "servicePhone": "15808319823"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_key(add_result, 'success')
        res_id = GetKey.get_key(add_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 修改-接口请求地址和请求数据
        update_url = "%s%s" % (Config().lyjc_host_debug, Config().lyjc_tourism_res_update_debug)
        update_data = {
            "id": res_id,
            "name": f"测试资源-更新-{res_id}",
            "area": "001001",
            "location": "洪川镇",
            "type": "008001",
            "servicePhone": "15808319823"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        update_result = Request.post_request(url=update_url, json=update_data, headers={"token": token})

        # 获取返回值
        response_update = GetKey.get_keys(update_result, 'body', 'code')
        success_update = GetKey.get_key(update_result, 'success')
        res_name_update = f"测试资源-更新-{res_id}"

        # 查看-接口其你去地址和请求数据
        select_url = f"%s%s/{res_id}" % (Config().lyjc_host_debug, Config().lyjc_tourism_res_select_id_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        select_result = Request.get_request(url=select_url, headers={"token": token})

        # 获取返回值
        response_select = GetKey.get_keys(select_result, 'body', 'code')
        success_select = GetKey.get_key(select_result, 'success')
        res_name_select = GetKey.get_keys(select_result, 'data', 'name')

        # 删除-接口请求地址和请求数据
        delete_url = f"%s%s/{res_id}" % (Config().lyjc_host_debug, Config().lyjc_tourism_res_delete_debug)

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        delete_result = Request.get_request(url=delete_url, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_key(delete_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-新增旅游资源'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步-修改旅游资源'):
            allure.attach(update_url, '请求地址')
            allure.attach(format_cn_res(update_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(update_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_update}\n'
                          f'success返回值为：{success_update}',
                          '实际结果')
        with allure.step('第三步-查看旅游资源'):
            allure.attach(select_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(select_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}\n'
                          f'name返回值为{res_name_update}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select}\n'
                          f'success返回值为：{success_select}\n'
                          f'name返回值为{res_name_select}',
                          '实际结果')
        with allure.step('第四步-删除旅游资源'):
            allure.attach(delete_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_delete}\n'
                          f'success返回值为：{success_delete}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_add == expect_response)
        assert_2 = assume(success_add == expect_success)
        assert_3 = assume(response_select == expect_response)
        assert_4 = assume(success_select == expect_success)
        assert_5 = assume(res_name_update in res_name_select)
        assert_6 = assume(response_update == expect_response)
        assert_7 = assume(success_update == expect_success)
        assert_8 = assume(response_delete == expect_response)
        assert_9 = assume(success_delete == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
                and assert_5
                and assert_6
                and assert_7
                and assert_8
                and assert_9
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_text_url}#资源列表', '接口文档地址-资源列表')
    @allure.feature('<行业监测>旅游资源模块')
    @allure.story('资源列表')
    @pytest.mark.lyjcTourismRes
    def test_tourism_resource_select_list_01(self, error_handling):
        """
         描述：资源列表
         请求路径：/tourismResource/list
         请求方式：POST
        """
        # 准备issue报告数据
        method_name = "旅游资源模块-资源列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().lyjc_host_debug, Config().lyjc_tourism_res_select_list_debug)
        data = {
            "current": 1,
            "size": 10
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_text_url}#区域列表', '接口文档地址-区域列表')
    @allure.feature('<行业监测>旅游资源模块')
    @allure.story('区域列表')
    @pytest.mark.lyjcTourismRes
    def test_tourism_resource_area_list_01(self, error_handling):
        """
         描述：资源列表
         请求路径：/tourismResource/areaList
         请求方式：GET
        """
        # 准备issue报告数据
        method_name = "旅游资源模块-区域列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().lyjc_host_debug, Config().lyjc_tourism_res_area_list_debug)

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        response = response[0] if isinstance(response, list) else response
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_text_url}#资源类型列表', '接口文档地址-资源类型列表')
    @allure.feature('<行业监测>旅游资源模块')
    @allure.story('资源类型列表')
    @pytest.mark.lyjcTourismRes
    def test_tourism_resource_res_type_list_01(self, error_handling):
        """
         描述：资源类型列表
         请求路径：/tourismResource/resourceTypeList
         请求方式：GET
        """
        # 准备issue报告数据
        method_name = "旅游资源模块-资源类型列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().lyjc_host_debug, Config().lyjc_tourism_res_type_list_debug)

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        response = response[0] if isinstance(response, list) else response
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
