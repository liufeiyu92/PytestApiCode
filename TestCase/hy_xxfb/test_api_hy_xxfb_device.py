"""
@File    : test_api_hy_xxfb_device.py
@Time    : 2019/9/23 9:53
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_xxfb import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestXxfbDevice:
    """
    信息发布-设备管理模块
    """
    api_device_text_url = (
        'http://192.168.10.200/hy/lyxxfb/hy-lyxxfb-information-issuing-platform/blob/master/doc/xxfb/doc/device.md')

    @allure.testcase(f'{api_device_text_url}#新增', '接口文档地址-新增设备')
    @allure.testcase(f'{api_device_text_url}#修改', '接口文档地址-修改设备')
    @allure.testcase(f'{api_device_text_url}#列表', '接口文档地址-查询设备')
    @allure.testcase(f'{api_device_text_url}#删除', '接口文档地址-删除设备')
    @allure.feature('<信息发布>设备管理模块')
    @allure.story('设备-增删查改')
    @pytest.mark.skip('设备为常量,不测增删改-2019.11.9')
    @pytest.mark.xxfbDevice
    def test_device_01(self, db_181, error_handling):
        """
        描述：新增设备-修改设备-查看设备是否修改成功-删除设备
        请求路径:
            · 新增设备 POST /lyxxfb/device/add
            · 修改设备 POST /lyxxfb/device/update
            · 查看设备 POST /lyxxfb/device/list
            · 删除设备 GET /lyxxfb/device/delete
        """
        # 准备issue报告数据
        method_name = "设备管理模块-设备增删查改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        sql = """SELECT `id` FROM f_device"""
        sql_result = read_sql(sql=sql, db_obj=db_181)
        device_id = random.choice(sql_result)[0]

        sql2 = """SELECT `id` FROM f_area"""
        sql_result = read_sql(sql=sql2, db_obj=db_181)
        area_id = random.choice(sql_result)[0]

        sql3 = f"""SELECT `name` FROM f_area WHERE `id`={area_id}"""
        sql_result = read_sql(sql=sql3, db_obj=db_181)
        area_name = random.choice(sql_result)[0]

        # 新增-接口请求地址和请求数据
        add_url = "%s%s" % (Config().xxfb_host_debug, Config().xxfb_device_add_debug)
        add_data = {
            "guidanceDisplayName": "测试诱导屏",
            "deviceId": f"{device_id}",
            "mac": "ac:1b:aw:2c:3q",
            "ip": "192.168.10.45",
            "pointName": "测试点位",
            "pointAddress": "测试点位地址",
            "areaId": area_id,
            "areaName": area_name,
            "longitude": "90",
            "latitude": "190"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_key(add_result, 'success')
        device_id_get = GetKey.get_key(add_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 修改-接口请求地址和请求数据
        update_url = "%s%s" % (Config().xxfb_host_debug, Config().xxfb_device_update_debug)
        update_data = {
            "guidanceDisplayName": f"测试诱导屏-更新-{device_id_get}",
            "deviceId": device_id,
            "mac": "365645646456",
            "ip": "368556565664566",
            "pointName": "西海岸",
            "pointAddress": "西海岸上方",
            "areaId": area_id,
            "areaName": area_name,
            "longitude": "90",
            "latitude": "190",
            "id": device_id_get
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        update_result = Request.post_request(url=update_url, json=update_data, headers={"token": token})

        # 获取返回值
        response_update = GetKey.get_keys(update_result, 'body', 'code')
        success_update = GetKey.get_key(update_result, 'success')
        device_name_update = f"测试诱导屏-更新-{device_id_get}"

        # 查看-接口其你去地址和请求数据
        select_url = "%s%s" % (Config().xxfb_host_debug, Config().xxfb_device_select_debug)
        select_data = {
            "mac": "",
            "areaId": area_id
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        select_result = Request.post_request(url=select_url, json=select_data, headers={"token": token})

        # 获取返回值
        response_select = GetKey.get_keys(select_result, 'body', 'code')
        success_select = GetKey.get_key(select_result, 'success')
        device_name_select = GetKey.get_keys(select_result, 'data', 'guidanceDisplayName')
        device_name_select = device_name_select[0] if isinstance(device_name_select, list) else device_name_select

        # 删除-接口请求地址和请求数据
        delete_url = "%s%s" % (Config().xxfb_host_debug, Config().xxfb_device_delete_debug)
        delete_data = {
            "id": device_id_get
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        delete_result = Request.get_request(url=delete_url, data=delete_data, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_key(delete_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-新增设备'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步-修改设备'):
            allure.attach(update_url, '请求地址')
            allure.attach(format_cn_res(update_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(update_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_update}\n'
                          f'success返回值为：{success_update}',
                          '实际结果')
        with allure.step('第三步-查看设备'):
            allure.attach(select_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(select_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}\n'
                          f'device_name返回值为{device_name_update}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select}\n'
                          f'success返回值为：{success_select}\n'
                          f'device_name返回值为{device_name_select}',
                          '实际结果')
        with allure.step('第四步-删除设备'):
            allure.attach(delete_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_delete}\n'
                          f'success返回值为：{success_delete}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_add == expect_response)
        assert_2 = assume(success_add == expect_success)
        assert_3 = assume(response_select == expect_response)
        assert_4 = assume(success_select == expect_success)
        assert_5 = assume(device_name_update in device_name_select)
        assert_6 = assume(response_update == expect_response)
        assert_7 = assume(success_update == expect_success)
        assert_8 = assume(response_delete == expect_response)
        assert_9 = assume(success_delete == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
                and assert_5
                and assert_6
                and assert_7
                and assert_8
                and assert_9
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
