"""
@File    : test_api_hy_xxfb_area.py
@Time    : 2019/9/18 10:58
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_xxfb import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestArea:
    """
    信息发布-区域管理模块
    """
    api_area_text_url = (
        'http://192.168.10.200/hy/lyxxfb/hy-lyxxfb-information-issuing-platform/blob/master/doc/xxfb/doc/area.md')

    @allure.testcase(f'{api_area_text_url}#新增', '接口文档地址-新增区域')
    @allure.testcase(f'{api_area_text_url}#修改', '接口文档地址-修改区域')
    @allure.testcase(f'{api_area_text_url}#列表', '接口文档地址-查询区域')
    @allure.testcase(f'{api_area_text_url}#删除', '接口文档地址-删除区域')
    @allure.feature('<信息发布>区域管理模块')
    @allure.story('区域-增删查改')
    @pytest.mark.xxfbArea
    def test_area_01(self, error_handling):
        """
        描述：新增区域-修改区域-查看区域是否修改成功-删除区域
        请求路径:
            · 新增区域 POST /lyxxfb/area/add
            · 修改区域 POST /lyxxfb/area/update
            · 查看区域 GET /lyxxfb/area/list
            · 删除区域 GET /lyxxfb/area/delete
        """
        # 准备issue报告数据
        method_name = "区域管理模块-区域增删查改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 新增-接口请求地址和请求数据
        add_url = "%s%s" % (Config().xxfb_host_debug, Config().xxfb_area_add_debug)
        add_data = {
            "name": "测试区域",
            "remark": "这是一条remark"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_key(add_result, 'success')
        area_id = GetKey.get_key(add_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 修改-接口请求地址和请求数据
        update_url = "%s%s" % (Config().xxfb_host_debug, Config().xxfb_area_update_debug)
        update_data = {
            "name": f"测试区域-更新-{area_id}",
            "remark": f"这是一条remark-更新-{area_id}",
            "id": f"{area_id}"
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        update_result = Request.post_request(url=update_url, json=update_data, headers={"token": token})

        # 获取返回值
        response_update = GetKey.get_keys(update_result, 'body', 'code')
        success_update = GetKey.get_key(update_result, 'success')
        area_name_update = f"测试区域-更新-{area_id}"
        area_remark_update = f"这是一条remark-更新-{area_id}"

        # 查看-接口其你去地址和请求数据
        select_url = "%s%s" % (Config().xxfb_host_debug, Config().xxfb_area_select_debug)
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        select_result = Request.get_request(url=select_url, headers={"token": token})

        # 获取返回值
        response_select = GetKey.get_keys(select_result, 'body', 'code')
        success_select = GetKey.get_key(select_result, 'success')
        area_name_select = GetKey.get_keys(select_result, 'data', 'name')
        area_name_select = area_name_select[0] if isinstance(area_name_select, list) else area_name_select
        area_remark_select = GetKey.get_keys(select_result, 'data', 'remark')

        # 删除-接口请求地址和请求数据
        delete_url = "%s%s" % (Config().xxfb_host_debug, Config().xxfb_area_delete_debug)
        delete_data = {
            "id": area_id
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        delete_result = Request.get_request(url=delete_url, data=delete_data, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_key(delete_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-新增区域'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步-修改区域'):
            allure.attach(update_url, '请求地址')
            allure.attach(format_cn_res(update_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(update_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_update}\n'
                          f'success返回值为：{success_update}',
                          '实际结果')
        with allure.step('第三步-查看区域'):
            allure.attach(select_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(select_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}\n'
                          f'area_name返回值为{area_name_update}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select}\n'
                          f'success返回值为：{success_select}\n'
                          f'area_name返回值为{area_name_select}',
                          '实际结果')
        with allure.step('第四步-删除区域'):
            allure.attach(delete_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_delete}\n'
                          f'success返回值为：{success_delete}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_add == expect_response)
        assert_2 = assume(success_add == expect_success)
        assert_3 = assume(response_select == expect_response)
        assert_4 = assume(success_select == expect_success)
        assert_5 = assume(area_name_update in area_name_select)
        assert_6 = assume(response_update == expect_response)
        assert_7 = assume(success_update == expect_success)
        assert_8 = assume(response_delete == expect_response)
        assert_9 = assume(success_delete == expect_success)
        assert_10 = assume(area_remark_update in area_remark_select)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
                and assert_5
                and assert_6
                and assert_7
                and assert_8
                and assert_9
                and assert_10
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
