"""
@File    : test_api_hy_lytc_parking_lot.py
@Time    : 2019/10/8 9:58
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_lytc import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestLytcParkingLot:
    """
    停车管理-停车基本信息模块
    """
    api_text_url = (
        'http://192.168.10.200/hy/lytc/hy-lytc-parking-lot-manage/blob/master/doc/resource-manage/doc/api-parking-lot.md')

    @allure.testcase(f'{api_text_url}#新增停车场', '接口文档地址-新增停车场')
    @allure.testcase(f'{api_text_url}#更新停车场', '接口文档地址-更新停车场')
    @allure.testcase(f'{api_text_url}#查询单个停车场', '接口文档地址-查询单个停车场')
    @allure.testcase(f'{api_text_url}#删除停车场', '接口文档地址-删除停车场')
    @allure.feature('<停车管理>停车基本信息模块')
    @allure.story('停车场-增删查改')
    @pytest.mark.lytcParkingLot
    def test_parking_lot_01(self):
        """
        描述：新增停车场-修改停车场-查看该停车场是否修改成功-删除停车场
        请求路径:
            · 新增停车场 POST /rm/parking-lot/save
            · 修改停车场 POST /rm/parking-lot/update
            · 查看停车场 GET /rm/parking-lot/get
            · 删除停车场 GET /rm/parking-lot/delete
        """
        # 准备issue报告数据
        method_name = "停车基本信息模块-停车场增删查改"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 新增-接口请求地址和请求数据
        add_url = "%s%s" % (Config().lytc_host_debug, Config().lytc_parking_lot_add_debug)
        add_data = {
            "name": "天府世界停车场",
            "regionCode": "001",
            "region": "洪川镇",
            "scenicAreaCode": "101",
            "scenicArea": None,
            "typeCode": "301",
            "type": "专用",
            "parkingSpaceNumber": "20",
            "contactsName": "刘飞宇",
            "contactsPhoneNumber": "15808319823",
            "address": "洪雅洪川镇",
            "dataSourceCode": "201",
            "dataSource": "自动获取",
            "longitude": 120,
            "latitude": 30
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        add_result = Request.post_request(url=add_url, json=add_data, headers={"token": token})

        # 获取返回值
        response_add = GetKey.get_keys(add_result, 'body', 'code')
        success_add = GetKey.get_key(add_result, 'success')
        parking_id = GetKey.get_key(add_result, 'data')
        expect_response = get_code(module_name)
        expect_success = True

        # 修改-接口请求地址和请求数据
        update_url = "%s%s" % (Config().lytc_host_debug, Config().lytc_parking_lot_update_debug)
        update_data = {
            "id": parking_id,
            "name": f"天府世界停车场-更新-{parking_id}",
            "regionCode": "001",
            "region": "洪川镇",
            "scenicAreaCode": "101",
            "scenicArea": None,
            "typeCode": "301",
            "type": "专用",
            "parkingSpaceNumber": "20",
            "contactsName": "刘飞宇",
            "contactsPhoneNumber": "15808319823",
            "address": "洪雅",
            "dataSourceCode": "201",
            "dataSource": "自动获取",
            "longitude": 120,
            "latitude": 30
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        update_result = Request.post_request(url=update_url, json=update_data, headers={"token": token})

        # 获取返回值
        response_update = GetKey.get_keys(update_result, 'body', 'code')
        success_update = GetKey.get_key(update_result, 'success')
        parking_name_update = f"天府世界停车场-更新-{parking_id}"

        # 查看-接口其你去地址和请求数据
        select_url = "%s%s" % (Config().lytc_host_debug, Config().lytc_parking_lot_select_debug)
        data = {
            "id": parking_id
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        select_result = Request.get_request(url=select_url, data=data, headers={"token": token})

        # 获取返回值
        response_select = GetKey.get_keys(select_result, 'body', 'code')
        success_select = GetKey.get_key(select_result, 'success')
        parking_name_select = GetKey.get_keys(select_result, 'data', 'name')

        # 删除-接口请求地址和请求数据
        delete_url = "%s%s" % (Config().lytc_host_debug, Config().lytc_parking_lot_delete_debug)
        delete_data = {
            "id": parking_id
        }
        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        delete_result = Request.get_request(url=delete_url, data=delete_data, headers={"token": token})

        # 获取返回值
        response_delete = GetKey.get_keys(delete_result, 'body', 'code')
        success_delete = GetKey.get_key(delete_result, 'success')

        # 报告执行步骤
        with allure.step('第一步-新增停车场'):
            allure.attach(add_url, '请求地址')
            allure.attach(format_cn_res(add_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(add_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_add}\n'
                          f'success返回值为：{success_add}',
                          '实际结果')
        with allure.step('第二步-修改停车场'):
            allure.attach(update_url, '请求地址')
            allure.attach(format_cn_res(update_data), '请求参数')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(update_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_update}\n'
                          f'success返回值为：{success_update}',
                          '实际结果')
        with allure.step('第三步-查看停车场'):
            allure.attach(select_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(select_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}\n'
                          f'interface_name返回值为{parking_name_update}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_select}\n'
                          f'success返回值为：{success_select}\n'
                          f'interface_name返回值为{parking_name_select}',
                          '实际结果')
        with allure.step('第四步-删除停车场'):
            allure.attach(delete_url, '请求地址')
            allure.attach(token, 'token')
            allure.attach(format_cn_res(delete_result['body']), '响应内容')
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response_delete}\n'
                          f'success返回值为：{success_delete}',
                          '实际结果')

        # 断言
        assert_1 = assume(response_add == expect_response)
        assert_2 = assume(success_add == expect_success)
        assert_3 = assume(response_select == expect_response)
        assert_4 = assume(success_select == expect_success)
        assert_5 = assume(parking_name_update in parking_name_select)
        assert_6 = assume(response_update == expect_response)
        assert_7 = assume(success_update == expect_success)
        assert_8 = assume(response_delete == expect_response)
        assert_9 = assume(success_delete == expect_success)
        check_bool = (
                assert_1
                and assert_2
                and assert_3
                and assert_4
                and assert_5
                and assert_6
                and assert_7
                and assert_8
                and assert_9
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
