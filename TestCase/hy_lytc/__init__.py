"""
@File    : __init__.py.py
@Time    : 2019/10/8 9:15
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import random
import sys
import pytest
import allure
import json
import time
from os import path
from Common import Consts
from Common.CreateTime import HandleTime
from Common.ConnectMysql import read_sql
from Common.Request import Request
from Common.GetKey import GetKey
from Common.ExecuteJson import *
from Common.ConnectJira import *
from ProjectEnum import *
from Settings.Config import Config
from pytest_assume.plugin import assume

module_name = 'lytc'
data_module_name = 'hy_lytc_data'
