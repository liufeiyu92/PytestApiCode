"""
@File    : test_api_hy_lytc_parking_realtime.py
@Time    : 2019/10/8 11:36
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from TestCase.hy_lytc import *

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


@pytest.mark.usefixtures('setup')
class TestLytcParkingRealTime:
    """
    停车管理-车位饱和度实时信息
    """
    api_text_url = (
        'http://192.168.10.200/hy/lytc/hy-lytc-parking-lot-manage/blob/master/doc/resource-manage/doc/api-parking-space-realtime.md')

    @allure.testcase(f'{api_text_url}#查询车位饱和度实时信息列表', '接口文档地址-查询车位饱和度实时信息列表')
    @allure.feature('<停车管理>车位饱和度实时信息')
    @allure.story('查询车位饱和度实时信息列表')
    @pytest.mark.lytcParkingRealTime
    def test_parking_realtime_select_01(self):
        """
        描述：查询车位饱和度实时信息列表
        请求路径：/rm/parking-space-realtime/list
        请求方式：POST
        """
        # 准备issue报告数据
        method_name = "车位饱和度实时信息-查询车位饱和度实时信息列表"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().lytc_host_debug, Config().lytc_parking_realtime_select_debug)
        data = {
            "pageNum": 1,
            "pageSize": 10
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.post_request(url=url, json=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_text_url}#查询区域景区车位实时信息', '接口文档地址-查询区域景区车位实时信息')
    @allure.feature('<停车管理>车位饱和度实时信息')
    @allure.story('查询区域景区车位实时信息')
    @pytest.mark.lytcParkingRealTime
    def test_parking_realtime_summary_01(self):
        """
        描述：查询区域景区车位实时信息
        请求路径：/rm/parking-space-realtime/summary/list
        请求方式：GET
        """
        # 准备issue报告数据
        method_name = "车位饱和度实时信息-查询区域景区车位实时信息"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 准备数据
        # 选择景区或区域
        my_type = random.choice(["region", "scenic_area"])

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().lytc_host_debug, Config().lytc_parking_summary_debug)
        data = {
            "type": my_type
        }

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, data=data, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(format_cn_res(data), '请求参数')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )

    @allure.testcase(f'{api_text_url}#获取所有景区内的所有停车场饱和度统计', '接口文档地址-获取所有景区内的所有停车场饱和度统计')
    @allure.feature('<停车管理>车位饱和度实时信息')
    @allure.story('获取所有景区内的所有停车场饱和度统计')
    @pytest.mark.lytcParkingRealTime
    def test_parking_realtime_space_select_all_01(self):
        """
        描述：获取所有景区内的所有停车场饱和度统计
        请求路径：/rm/parking-space-realtime/scenic-area-parking-lot-saturation/get
        请求方式：GET
        """
        # 准备issue报告数据
        method_name = "车位饱和度实时信息-获取所有景区内的所有停车场饱和度统计"
        project_id = get_project_id(module_name)
        summary = GetJiraData.summary_format(method_name)
        description = GetJiraData.description_format()
        assignee = get_assignee(module_name)

        # 接口请求地址和请求数据
        url = "%s%s" % (Config().lytc_host_debug, Config().lytc_parking_select_all_debug)

        token = Config().get_conf(Config.TITLE_DEBUG, Config.VALUE_TOKEN)

        # 发起请求
        result = Request.get_request(url=url, headers={"token": token})

        # 获取返回值
        response = GetKey.get_keys(result, 'body', 'code')
        success = GetKey.get_keys(result, 'body', 'success')
        expect_response = get_code(module_name)
        expect_success = True

        # 报告执行步骤
        with allure.step('请求地址和请求数据'):
            allure.attach(url, '请求地址')
            allure.attach(token, 'token')
        with allure.step('接口响应内容'):
            allure.attach(format_cn_res(result['body']), '响应内容')
        with allure.step('检查结果'):
            allure.attach(f'code码返回值为：{expect_response}\n'
                          f'success返回值为：{expect_success}',
                          '期望结果')
            allure.attach(f'code码返回值为：{response}\n'
                          f'success返回值为：{success}',
                          '实际结果')

        # 断言
        assert_1 = assume(response == expect_response)
        assert_2 = assume(success == expect_success)
        check_bool = (
                assert_1
                and assert_2
        )

        # 将错误的接口数据传入全局列表里
        if not check_bool:
            Request.save_error_api_data(
                pro_id=project_id,
                summary=summary,
                description=description,
                assignee=assignee
            )
