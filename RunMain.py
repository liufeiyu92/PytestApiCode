"""
@File    : RunMain.py
@Time    : 2019/7/15 17:13
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import sys
import pytest
import Common.Consts
from Common import LogCof
from Common import Shell
from Common.ConnectJira import *
from Settings import Config

if __name__ == '__main__':
    conf = Config.Config()
    log = LogCof.MyLog()
    log.info_msg('初始化配置文件, path=' + conf.conf_path)

    shell = Shell.Shell()
    xml_report_path = conf.xml_report_path
    html_report_path = conf.html_report_path

    # 定义测试集
    allure_list = '--allure_features=test-name'

    args = ['-s', '-q', '--no-print-logs', '--alluredir', xml_report_path]
    log.info_msg('执行用例集为：%s' % allure_list)
    # self_args = sys.argv[1:]
    pytest.main(args)
    cmd = 'allure generate %s -o %s' % (xml_report_path, html_report_path)
    try:
        shell.invoke(cmd)
        if conf.jira_issue_switch == "1":
            error_num = Common.Consts.API_RESPONSE_NUMBER
            true_num = Common.Consts.API_TRUE_RESPONSE_NUMBER
            accuracy_rate = "%.2f" % (true_num / (true_num + error_num))
            # 自动提交问题单,如果错误接口数量超过10或者错误率低于95%,则不自动提交问题单，否则自动提交问题单
            if Common.Consts.API_RESPONSE_NUMBER >= 10 or float(accuracy_rate) < 0.95:
                pass
            else:
                for error in Common.Consts.API_ERROR_RESPONSE_DATA:
                    GetJiraData().create_issue(
                        project_id=error['project_id'],
                        summary=error['summary'],
                        description=error['description'],
                        assignee=error['assignee'])

    except Exception:
        log.error_msg('执行用例失败，请检查环境配置')
        raise
