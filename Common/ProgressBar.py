"""
@File    : ProgressBar.py
@Time    : 2019/8/2 14:10
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import time
import sys


def progress_test(length=10, refresh_sec=0.1):
    """
    进度条
    :param length: 控制进度条长度
    :param refresh_sec: 控制进度条增长速度
    :return:
    """
    bar_length = length
    for percent in range(0, 101):
        hashes = '■' * int(percent / 100.0 * bar_length)
        spaces = ' ' * (bar_length - len(hashes))
        sys.stdout.write("\rProgress: [%s] %d%%" % (hashes + spaces, percent))
        sys.stdout.flush()
        time.sleep(refresh_sec)


progress_test()
