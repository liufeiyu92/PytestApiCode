"""
@File    : ExecutePDF.py
@Time    : 2019/7/23 14:19
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import sys
import pdfplumber

path = 'testdemo.pdf'
pdf = pdfplumber.open(path)

for page in pdf.pages:
    # 获取当前页面的全部文本信息，包括表格中的文字
    # print(page.extract_text())
    # print(page.extract)

    for table in page.extract_tables():
        # print(table)
        for row in table:
            print(row)
        print('---------- 分割线 ----------')

pdf.close()

if __name__ == '__main__':
    # cpu = sys.argv[1]
    # memory = sys.argv[2]
    # yp = sys.argv[3]
    # print(f'cpu为:{cpu}  内存为:{memory}  硬盘为:{yp}')
    pass
