"""
@File    : Consts.py
@Time    : 2019/7/15 14:39
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
@Remark  : 该Py文件提供全局变量配置
"""
# 接口全局配置
API_ENVIRONMENT_LEVEL = 'debug'  # 控制打印日志的级别 [debug info warning error critical]
API_ENVIRONMENT_SWITCH = 1  # 控制打印日志开关,默认为打开 1.打开 0.关闭
API_ENVIRONMENT_RELEASE = 'release'

# 接口相应时间列表,单位毫秒
API_RESPONSE_TIME_LIST = []

# 接口执行结果列表
API_RESPONSE_LIST = []

# 洪雅交通管理平台-用户名字列表
API_USER_NAME_LIST = []

# 洪雅交通管理平台-单位ID列表
API_COMPANY_ID_LIST = []

# 洪雅交通管理平台-角色ID列表
API_ROLE_ID_LIST = []

# 洪雅交通管理平台-角色名称列表
API_ROLE_NAME_LIST = []

# 接口测试结果，错误数量
API_RESPONSE_NUMBER = 0
API_TRUE_RESPONSE_NUMBER = 0

# 接口错误数据
API_ERROR_RESPONSE_DATA = []
