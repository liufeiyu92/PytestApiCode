"""
@File    : Shell.py
@Time    : 2019/7/18 14:22
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""

import subprocess
import os


class Shell:
    @staticmethod
    def invoke(cmd):
        output, errors = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        shell_data = output.decode("utf-8")
        return shell_data
