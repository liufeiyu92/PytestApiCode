"""
@File    : LogCof.py
@Time    : 2019/7/16 9:09
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""

import logging
import os
import time
import platform
from Common.Consts import API_ENVIRONMENT_LEVEL

LEVELS = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL
}
path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
log_file = path + '/Log/log.log'
# err_file = path + '/Log/error.log'
LOG_FORMAT = "%(message)s "  # 配置输出日志格式
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'


def get_current_time():
    return time.strftime(DATE_FORMAT, time.localtime(time.time()))


class MyLog:

    def __init__(self):
        if platform.system() == "Linux":
            self.log_path = log_file
        elif platform.system() == "Windows":
            self.log_path = log_file

        self.logfile = self.log_path
        logging.basicConfig(level=LEVELS.get(API_ENVIRONMENT_LEVEL),
                            format=LOG_FORMAT,
                            filemode='a+',
                            datefmt=DATE_FORMAT,
                            filename=self.logfile
                            )

    # 日志记录
    @staticmethod
    def debug_msg(msg):
        msg = msg if isinstance(msg, str) else str(msg)
        logging.debug("[DEBUG " + get_current_time() + "] " + msg)

    @staticmethod
    def info_msg(msg):
        msg = msg if isinstance(msg, str) else str(msg)
        logging.debug("[INFO " + get_current_time() + "] " + msg)

    @staticmethod
    def warning_msg(msg):
        msg = msg if isinstance(msg, str) else str(msg)
        logging.debug("[WARNING " + get_current_time() + "] " + msg)

    @staticmethod
    def error_msg(msg):
        msg = msg if isinstance(msg, str) else str(msg)
        logging.debug("[ERROR " + get_current_time() + "] " + msg)

    @staticmethod
    def critical_msg(msg):
        msg = msg if isinstance(msg, str) else str(msg)
        logging.debug("[CRITICAL " + get_current_time() + "] " + msg)


if __name__ == "__main__":
    pass
