"""
@File    : ConnectJenkins.py
@Time    : 2019/9/7 16:42
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import jenkins
from Settings.Config import Config
from typing import Union

_default_job_name = "洪雅智慧旅游自动化测试报告"


class ConnectJenkins:
    """操作jenkins类-暂未封装增删"""

    def __init__(self, job_name=_default_job_name):
        timeout = 1
        self._jenkins_url = Config().jenkins_url  # 链接地址
        self._username = Config().jenkins_username  # 用户名
        self._password = Config().jenkins_password  # 密码
        self._api_password = Config().jenkins_api_password  # api-token密码
        self.job_name = job_name
        self._jk_server = jenkins.Jenkins(
            url=self._jenkins_url,
            username=self._username,
            password=self._api_password,
            timeout=timeout
        )

    def version(self) -> str:
        """获取jenkins版本号"""
        version = self._jk_server.get_version()
        return version

    def user_info(self) -> dict:
        """获取登录用户详细信息"""
        return self._jk_server.get_whoami()

    def build_job(self, job_name: str, param: dict = None) -> int:
        """
        立即构建job名为job_name的job
        :return 返回queueNum提供给方法get_queue_item使用
        """
        return self._jk_server.build_job(job_name, param)

    def queue_item(self, queue_num: int) -> dict:
        """
        获取有关排队项目（即将创建的作业）的信息
        :param queue_num: 队列号(由方法build_job返回)
        :return:排队信息字典
        """
        return self._jk_server.get_queue_item(queue_num)

    def debug_job_info(self, job_name: str) -> dict:
        """
        以更易读的格式获取工作信息
        :param job_name:job_name
        :return:工作信息词典
        """
        return self._jk_server.debug_job_info(job_name)

    def job_info(self, job_name: str) -> dict:
        """
        获取job_name的job的详细信息
        :param job_name: 项目名
        :return:工作信息字典
        """
        return self._jk_server.get_job_info(job_name)

    def job_num_build_state(self, build_num: int) -> str:
        """
        获取job名为job_name的job的某次(build_num)构建的执行结果状态
        :param build_num:构建的num
        :return: 状态
        """
        return self._jk_server.get_build_info(self.job_name, build_num)['result']

    def job_last_build_number(self, job_name: str) -> int:
        """
        获取job名为job_name的job的最后次构建号
        :param job_name: 项目名
        :return: 构建num
        """
        return self._jk_server.get_job_info(job_name)['lastBuild']['number']

    def job_next_build_number(self, job_name: str) -> int:
        """
        获取job名为job_name的job的下一次构建号
        :param job_name:项目名
        :return:构建num
        """
        return self._jk_server.get_job_info(job_name)['nextBuildNumber']

    def job_is_building(self, job_name: str, build_num: int) -> bool:
        """
        判断job名为job_name的job的某次构建是否还在构建中
        :param job_name:
        :param build_num:
        :return:
        """
        return self._jk_server.get_build_info(job_name, build_num)['building']

    def build_info(self, job_name: str, build_num: int) -> dict:
        """
        获取构建信息字典
        :param job_name:构建job名称
        :param build_num:job内的构建num
        :return:构建信息字典
        """
        return self._jk_server.get_build_info(job_name, build_num)

    def build_test_report(self, build_num) -> Union[None, dict]:
        """
        获取job名为job_name的job的某次(build_num)的测试报告
        :param build_num: 构建num
        :return: 测试报告结果字典,如果没有测试报告则为None
        """
        return self._jk_server.get_build_test_report(self.job_name, build_num)

    def allure_report_url(self, project_name):
        next_num = self.job_next_build_number(project_name) - 1
        url = f"http://192.168.10.45:8080/job/{project_name}/{next_num}/allure/"
        return url

    def get_version(self):
        """打印jenkins版本号"""
        print(self.version())

    def get_user_info(self):
        """打印登录用户详细信息"""
        print(self.user_info())

    def get_job_info(self, job_name):
        """打印job_name的job的详细信息"""
        print(self.job_info(job_name))

    def get_queue_item(self, queue_num: int):
        """打印有关排队项目（即将创建的作业）的信息"""
        print(self.queue_item(queue_num))

    def get_build_info(self, job_name: str, build_num: int):
        """打印构建信息字典"""
        print(self.build_info(job_name, build_num))

    def get_job_num_build_state(self, build_num):
        """打印job名为job_name的job的某次(build_num)构建的执行结果状态"""
        print(self.job_num_build_state(build_num))

    def get_job_last_build_number(self, job_name):
        """打印job名为job_name的job的最后次构建号"""
        print(self.job_last_build_number(job_name))

    def get_job_is_building(self, job_name: str, build_num: int):
        """打印job名为job_name的job的某次构建是否还在构建中"""
        print(self.job_is_building(job_name, build_num))


if __name__ == '__main__':
    jk = ConnectJenkins()
    jk.get_version()
    num = jk.job_next_build_number(_default_job_name)
    print(num)
    """
    """
    # print(type(jk.job_is_building(_default_job_name, 193)))
    # v判断job名为job_name的job的某次构建是否还在构建中
    # print(jk.build_job('洪雅智慧旅游自动化测试报告'))
    # print(type(jk.job_num_build_state(195)))
    # jk.get_user_info()
    # print(jk.job_num_build_state(195))
