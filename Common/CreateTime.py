"""
@File    : CreateTime.py
@Time    : 2019/7/18 16:35
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import time
import datetime


class HandleTime:
    # 1.把datetime转成字符串
    @staticmethod
    def datetime_to_string(dt):
        return dt.strftime("%Y-%m-%d %H:%M:%S")

    # 2.把字符串转成datetime
    @staticmethod
    def string_to_datetime(st):
        return datetime.datetime.strptime(st, "%Y-%m-%d %H:%M:%S")

    # 3.把字符串转成时间戳形式
    @staticmethod
    def string_to_timestamp(st):
        return time.mktime(time.strptime(st, "%Y-%m-%d %H:%M:%S"))

    # 4.把时间戳转成字符串形式
    @staticmethod
    def timestamp_to_string(sp):
        return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(sp))

    @staticmethod
    def timestamp_to_string_report(sp):
        return time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime(sp))

    # 5.把datetime类型转外时间戳形式
    @staticmethod
    def datetime_to_timestamp(dt):
        return time.mktime(dt.timetuple())


if __name__ == '__main__':
    # 日期时间字符串
    st = "2017-11-23 16:10:10"
    # 当前日期时间
    dt = datetime.datetime.now()
    # 当前时间戳
    sp = time.time()
    # 1.把datetime转成字符串
    HandleTime.datetime_to_string(dt)
    # 2.把字符串转成datetime
    HandleTime.string_to_datetime(st)
    # 3.把字符串转成时间戳形式
    HandleTime.string_to_timestamp(st)
    # 4.把时间戳转成字符串形式
    HandleTime.timestamp_to_string(sp)
    # 5.把datetime类型转外时间戳形式
    HandleTime.datetime_to_timestamp(dt)
