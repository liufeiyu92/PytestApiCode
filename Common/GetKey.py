"""
@File    : GetKey.py
@Time    : 2019/7/16 15:20
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""

from jsonpath import jsonpath


class GetKey:
    """
    该类提供对json格式返回值提供了值的提取
    """

    @staticmethod
    def get_key(data, key):
        """
        获取单个key的值
        :param data: 数据源
        :param key: 键
        :return: 键对应的值
        """
        # if key in data.keys():
        #     return data[key]
        # else:
        #     for k, v in data.items():
        #         if isinstance(v, list):
        #             for i in v:
        #                 result = self.get_key(i, key)
        #                 if result or result == 0 or (result is not None and result is not False):
        #                     return result
        #
        #         elif isinstance(v, dict):
        #             result = self.get_key(v, key)
        #             if result or result == 0 or (result is not None and result is not False):
        #                 return result
        result = jsonpath(data, f'$..{key}')
        if result:
            if len(result) < 2:
                return result[0]
            if len(result) >= 2:
                return result

    @staticmethod
    def get_keys(data, tag, key):
        """
        如果不同父键下面有同样名称的子键，使用该调用
        :param data: 数据源
        :param tag: 父键
        :param key: 子键
        :return: 子键对应值
        """
        result = jsonpath(data, f'$..{tag}..{key}')
        if result:
            if len(result) < 2:
                return result[0]
            if len(result) >= 2:
                return result


if __name__ == '__main__':
    a = True
    assert a == 0
