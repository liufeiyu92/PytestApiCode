"""
@File    : ConnectMysql.py
@Time    : 2019/8/20 15:24
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import pandas as pd


def read_sql(sql, db_obj):
    res = pd.read_sql(sql, db_obj)
    return res.values.tolist()


if __name__ == '__main__':
    # api_complaint_url = (
    #     'http://192.168.10.200/hy/lysh/hy-lysh-merchant-manage/blob/master/doc/api-merchant-complaint-record.md')
    # print(api_complaint_url)
    pass
