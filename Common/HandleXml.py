"""
@File    : HandleXml.py
@Time    : 2019/7/16 15:23
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import xmltodict
import json


def xml_to_json(xml_file) -> str:
    """
    提供xml数据转换为json格式数据
    :param xml_file: xml文件地址
    :return:json格式数据(str)
    """
    # 读取xml文件
    with open(xml_file, 'r', encoding='utf-8') as fp:
        xml_data = fp.read()
    # parse是的xml解析器
    xml_parse = xmltodict.parse(xml_data)
    # json.dumps()是将dict转化成json格式，loads()是将json转化成dict格式。
    # dumps()方法的ident=1，格式化json
    # encode('utf-8').decode('unicode_escape') 是将\uXXX字符转换为正常显示中文
    jsonstr = json.dumps(xml_parse, indent=2).encode('utf-8').decode('unicode_escape')
    return jsonstr


def xml_to_dict(xml_file) -> dict:
    """
    提供xml数据转换为dict可处理格式数据
    :param xml_file: xml文件地址
    :return:dict格式数据(dict)
    """
    # 读取xml文件
    with open(xml_file, 'r', encoding='utf-8') as fp:
        xml_data = fp.read()
    # parse是的xml解析器
    xml_parse = xmltodict.parse(xml_data)
    # json.dumps()是将dict转化成json格式，loads()是将json转化成dict格式。
    # dumps()方法的ident=1，格式化json
    # encode('utf-8').decode('unicode_escape') 是将\uXXX字符转换为正常显示中文
    jsonstr = json.dumps(xml_parse, indent=2).encode('utf-8').decode('unicode_escape')
    return json.loads(jsonstr)


if __name__ == '__main__':
    print(xml_to_json('xmldemo.xml'))
