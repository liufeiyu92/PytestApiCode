"""
@File    : ExecuteJson.py
@Time    : 2019/8/12 11:55
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import demjson


def format_cn_res(data, indent=3):
    # return json.dumps(data, indent=indent).encode('utf-8').decode('unicode_escape')
    return demjson.encode(data, indent_amount=indent, compactly=False).encode('utf-8').decode('unicode_escape')
