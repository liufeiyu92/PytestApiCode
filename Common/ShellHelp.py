"""
@File    : ShellHelp.py
@Time    : 2019/10/15 16:05
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
import os

root_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
file_dir = root_path + "/TestCase"


def my_help():
    """
    - help
    """
    print("- show : 查询可执行的模块名\n"
          "- help : 查询执行参数\n"
          "- all : 执行所有模块的测试用例\n"
          "- 第一个参数 : 环境地址 说明<传入完整的地址,需要有http://>\n"
          "- 第二个参数 : 模块名\n"
          "- 第三个参数 : 自动提交问题单开关 1代表打开 0代表关闭 不传默认为0\n"
          "- 请求示例 : python RunCmd.py http://192.168.10.45 hy_lysh 1")


def show_module():
    """
    - show
    """
    test_case_module = os.listdir(file_dir)
    test_case_module.remove('__pycache__')
    test_case_module.remove('conftest.py')
    test_case_module.remove('__init__.py')
    return test_case_module


if __name__ == '__main__':
    pass