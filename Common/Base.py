"""
@File    : Base.py
@Time    : 2019/11/19 16:10
@Author  : LiuFeiYu
@Email   : Liufeiyu@sunrise.net
@Software: PyCharm
"""
from abc import ABCMeta, abstractmethod
from typing import Union


class Base(metaclass=ABCMeta):
    """
    Base基类，禁止直接调用，请务必继承此基类后重写方法
    """
    @staticmethod
    @abstractmethod
    def get_request(url: str, data: dict = None, **kwargs) -> Union[dict, str]:
        """
        Get请求
        """
        pass

    @staticmethod
    @abstractmethod
    def post_request(url: str, data: dict = None, json: dict = None, **kwargs) -> Union[dict, str]:
        """
        Post请求
        """
        pass

    @staticmethod
    @abstractmethod
    def post_request_multipart(url, data, header, file_parm, file, f_type) -> Union[dict, str]:
        """
        提交Multipart/form-data 格式的Post请求
        """
        pass

    @staticmethod
    @abstractmethod
    def put_request(url: str, data: dict = None, json: dict = None, **kwargs) -> Union[dict, str]:
        """
        Put请求
        :return:

        """
        pass

    @staticmethod
    @abstractmethod
    def delete_request(url: str, data: dict = None, json: dict = None, **kwargs) -> Union[dict, str]:
        """
        Delete请求
        :return:

        """
        pass

    @staticmethod
    @abstractmethod
    def response_error_num(state: bool):
        """
        全局变量-接口错误数量
        :param state: 每次接口是否错误参照标准
        :return:
        """
        pass

    @staticmethod
    @abstractmethod
    def save_error_api_data(pro_id, summary, description, assignee):
        """
        储存接口错误信息
        :param pro_id:项目id
        :param summary:主题
        :param description:描述
        :param assignee:报告人
        :return:
        """
        pass