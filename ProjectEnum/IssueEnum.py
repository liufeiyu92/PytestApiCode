"""
@File    : IssueEnum.py
@Time    : 2019/9/26 17:47
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from enum import Enum


class IssueStatus(Enum):
    """问题单状态枚举类"""
    # 待办
    ToDo = "11"
    # 处理中
    InProgress = "21"
    # 完成
    Done = "31"
