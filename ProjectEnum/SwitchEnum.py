"""
@File    : SwitchEnum.py
@Time    : 2019/9/26 16:38
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from enum import Enum, unique


@unique
class SwitchEnum(Enum):
    """开关类枚举"""
    # 开
    ON = 1
    # 关
    OFF = 0
