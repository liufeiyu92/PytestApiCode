"""
@File    : DateEnum.py
@Time    : 2019/9/26 17:05
@Author  : LiuFeiYu
@Email   : liufeiyu@sunrise.net
@Software: PyCharm
"""
from enum import Enum, IntEnum, unique


@unique
class GraininessEnum(IntEnum):
    """车流量统计枚举"""
    # 时
    HOUR = 1
    # 天
    DAY = 2
    # 月
    MONTH = 3


@unique
class DateEnum(Enum):
    """日期类统计枚举"""
    # 秒
    SECOND = 1
    # 分
    MINUTE = 2
    # 时
    HOUR = 3
    # 天
    DAY = 4
    # 月
    MONTH = 5
    # 年
    YEAR = 6
