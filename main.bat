@echo off
if "%1" == "max" goto top
start "" /max "%~nx0" max
exit
:top

echo [step 0] - initialize report parameters
set reruns=3
set test_target=.\TestCase
set allure_target=.\Report\xml\
set allure_report=.\Report\html\

echo [step 1] - remove all documents in allure report root
del /a /f /q %allure_report%

echo [step 2] - starting pytest
pytest %test_target% --reruns %reruns% --alluredir=%allure_target%

echo [step 3] - finished pytest and starting generate allure report
call allure generate -c %allure_target% -o %allure_report%

echo [step 4] - open allure report by jetty
allure open %allure_report%

echo [step 5] - report has been generated successfully!

echo press any key to exit...
pause & exit